WorkWatcher jest systemem służącym do nadzorowania i śledzenia postępów pracy
pracowników firmy. Jest narzędziem, które pomaga w ocenie wydajności i wartości
pracy poszczególnych pracowników.

System ułatwia kontrolowanie pracowników. Przy regularnym sprawdzaniu raportów
po pewnym czasie można zweryfikować:

- czy pracownik przykłada się do pracy
- czy cały dostępny czas poświęca na pracę
- czy w czasie pracy nie zajmuje się wykonywaniem innych działań z nią nie związanych

System składa się z dwóch części - **aplikacji klienta** oraz **aplikacji
serwerowej**.

**Aplikacja klienta** jest instalowana na komputerach, które mają podlegać
nadzorowaniu. Program wysyła na serwer zrzut ekranu co określoną w konfiguracji
ilość sekund.

**Aplikacja serwerowa** pracuje na centralnym serwerze, gdzie aplikacje
klienckie wysyłają dane. Ta aplikacja udostępnia interfejs użytkownika w którym
można przeprowadzić analizę nadesłanych danych.

Opis aplikacji serwerowej
=========================

W referencyjnym systemie wirtualnym hasło użytkownika "root" to "workwatcher".
Zaleca się zmianę tego hasła za pomocą komendy passwd po zalogowaniu się do
systemu przez ssh.

Logowanie
---------

Do aplikacji serwerowej można się zalogować klikając link "**Logowanie**"
w prawym górnym rogu okna. Następnie należy podać nazwę użytkownika i hasło.
Z aplikacji serwerowej mogą korzystać dwa typy użytkowników:

- **administratorzy** - ten typ użytkowników ma najbardziej rozległe
uprawnienia i ma dostęp do wszystkich elementów systemu

- **obserwatorzy** - ten typ użytkowników ma uprawnienia potrzebne do
przeglądania raportów i tworzenia notatek

Do normalnego korzystania z aplikacji serwerowej zaleca się używanie konta
obserwatora. Standardowo w systemie po instalacji są dostępne dwa konta

::

  użytkownik      hasło
  -------------   -------------
  admin           admin
  observer        observer

Po instalacji systemu zaleca się zmianę standardowych haseł.

Zarządzanie użytkownikami
-------------------------

Użytkownicy o uprawnieniach administratora mają dostęp do listy użytkowników
systemu dostępnej w menu "Konto". Znajduje się ono w prawym górnym rogu
serwisu, po kliknięciu którego należy wybrać "Zarządzanie użytkownikami".

Istnieje możliwość dodawania nowych kont użytkowników oraz edycji istniejacych.

Przy tworzeniu konta poza standardowymi danymi, takimi jak:
**nazwa użytkownika**, **e-mail** i **hasło** należy też wybrać **uprawnienia**
oraz **grupy**.

**Uprawnienia** określają do jakich funkcji ma dostęp konkretny użytkownik.
Administrator ma kontrolę nad całym systemem. Obserwator może przeglądać
raporty i sporządzać notatki.

**Grupy** określają do których grup w systemie należy użytkownik. Przydział do
konkretnych grup ma znaczenie dla użytkowników z uprawnieniem obserwatora,
ponieważ od tego zależy jakie komputery będą oni widzieli na liście komputerów.

Zarządzanie grupami
-------------------

Użytkownicy o uprawnieniach administratora mają dostęp do listy grup systemu
dostępnej w menu "Konto". Znajduje się ono w prawym górnym rogu serwisu,
po kliknięciu którego należy wybrać "Zarządzanie grupami".

Istnieje możliwość dodawania nowych grup do systemu oraz edycji istniejących.

Przy tworzeniu grupy musimy podać jej nazwę. Ta nazwa będzie widoczna w dwóch
miejscach:

- przy tworzeniu lub edycji konta użytkownika istnieje możliwość przypisania
użytkownika do utworzonej grupy

- przy dodawaniu do systemu komputera lub edycji jego ustawień należy przypisać
go do jakiejś grupy

Lista komputerów
----------------

Lista komputerów zawiera wszystkie komputery działające w systemie. Dzięki tej
liście możemy wyświetlić **raporty** oraz **zestawienie notatek** dla danego
komputera.

Użytkownik o uprawnieniach obserwatora widzi na liście komputerów tylko te
komputery, które są przypisane do grup do których należy użytkownik. Czyli
na przykład w systemie może być zarejestrowanych dwóch obserwatorów - pierwszy
przypisany do grupy "dział produkcji" a drugi przypisany do grupy "dział
administracji". Pierwszy na liście komputerów będzie widział tylko te
przypisane do działu produkcji a drugi obserwator będzie widział tylko te
przypisane do działu administracji. Użytkownik może być przypisany do dowolnej
ilości grup.

Użytkownik o uprawnieniach administratora ma dodatkowe możliwości:

- widzi na liście wszystkie komputery zarejestrowane w systemie

- może dodawać **nowe komputery**

- może edytować ustawienia komputera

- może usuwać komputery z systemu

- ma dostęp do pełnej listy komputerów na której są wyświetlane ID i klucze

Dodawanie nowego komputera do systemu i edycja ustawień istniejącej maszyny
---------------------------------------------------------------------------

Aby dodać nowy komputer do systemu trzeba zalogować się na konto o
uprawnieniach administratora i następie na liście komputerów kliknąć link
"Nowy komputer" znajdujący się w prawym górnym rogu listy.

W formie dodawania komputera w zakładce "Ustawienia podstawowe" musimy
podać cztery informacje.

**Nazwa komputera** ma znaczenie dla operatorów systemu - ułatwia
identyfikację maszyny. Dobrą nazwą maszyny może być np. "Piętro 1/pokój 2/
komputer 4 - na którym najczęściej pracuje pani Gosia".

**Klucz** jest używany przy wysyłaniu raportów z konkretnego komputera. Jest
czymś w rodzaju hasła dla danego komputera. Standardowo klucz jest losowo
generowany i można go zmienić według własnego uznania. Klucz powinien się
składać wyłącznie z liter i cyfr.

**Opis komputera** ma znaczenie dla operatorów systemu. W tym polu można podać
dodatkowe informacje na temat lokalizacji komputera oraz godzin pracy
konkretnych osób np. "Na tym komputerze od godziny 8:00 do 12:00 pracuje pani
Ala z działu produkcji. Od godziny 12:00 do 16:00 pracuje pan Piotrek z
marketingu - on może korzystać z serwisów społecznościowych."

**Grupa** do której jest przypisany komputer. W liście komputerów, które widzą
użytkownicy z uprawnieniem obserwatora są wyświetlane tylko komputery powiązane
z grupami do których należy obserwator.

Dodatkowo w zakładce "Ustawienia zaawansowane" możemy zmienić ustawienia
aplikacji klienta.

**Nadpisywanie konfiguracji klienta** jest parametrem, który określa czy
aplikacja klienta ma pobierać parametry konfiguracyjne z serwera. Standardowo
aplikacja klienta korzysta z ustawień zapisanych w pliku **ww.ini**, jednak
może dojść do sytuacji, że administrator systemu będzie chciał poddać danego
użytkownika bardziej szczegółowej obserwacji. W wersji 1.0.x trzeba było
wprowadzać zmiany w pliku konfiguracyjnym aplikacji klienta na monitorowanym
komputerze. Od wersji 1.1.x aplikacja klienta potrafi pobrać nowe wartości
parametrów z konfiguracji zapisanej na serwerze.

**Wysyłanie raportów co** jest parametrem, który jest używany do nadpisywania
wartości parametru **delay** w konfiguracji aplikacji klienta. Ten parametr
mówi aplikacji klienta co ile sekund ma wysyłać raport. Jeśli ktoś chce bliżej
przyjrzeć się pracy monitorowanego pracownika, wystarczy zmniejszyć wartość
tego parametru do np. 15 sekund i przejść do trybu "Na żywo".

W czasie edycji ustawień komputera można zmienić powyższe dane.

Usuwanie komputerów
```````````````````

Administrator ma możliwość usuwania komputerów z systemu.

Lista komputerów do wydruku
```````````````````````````

Na liście komputerów do wydruku można znaleźć wszystkie informacje związane
z komputerami w systemie. Ta lista została stworzona aby ułatwić konfigurowanie
**aplikacji klienta** na komputerach. Można zapisać tę listę w pliku lub wysłać
mailem i następnie kopiować ID komputera oraz klucz do pliku konfiguracyjnego
konkretnego klienta.

Raporty
-------

Aplikacja klienta działająca na komputerach w systemie wysyła **raport** co
zadany czas. Raport składa się z kilku informacji:

- zrzut ekranu z komputera klienta

- data wykonania zrzutu ekranu (z komputera klienta)

- czas wykonania zrzutu ekranu (z komputera klienta)

- adres ip komputera z którego został wysłany raport

- nazwa hosta

- nazwa użytkownika zalogowanego w systemie klienta

Aby wyświetlić raporty dla danego komputera należy przejść do listy komputerów
i kliknąć "**Raporty**" przy wybranym komputerze. Domyślnie są
wyświetlane tylko raporty wysłane w dniu dzisiejszym. Aby wyświetlić raporty
wcześniejsze lub za wybrany okres należy ustawić odpowiednie daty w polach
wyszukiwania "Od" i "Do" i kliknąć "Szukaj". W zależności od konfiguracji
systemu okresowo są usuwane stare raporty do których nie utworzono żadnych
notatek.

Po kliknięciu na zrzut ekranu, obrazek zostanie wyświetlony w nowym oknie
- dzięki temu jest możliwość zobaczenia zrzutu w pełnej rozdzielczości.

Po prawej stronie przy każdym zrzucie ekranu są wyświetlane dodatkowe
informacje wchodzące w skład raportu.

Pod tymi informacjami jest wyświetlany link "Nowa notatka" lub tabelka w której
jest wyświetlana notatka wraz z linkiem służącym do jej edycji.

Dodawanie nowej notatki
```````````````````````

Notatki są sposobem oceniania raportów nadsyłanych z komputera pracownika.
Jeśli nadsyłane zrzuty ekranu dokumentują normalną pracę jaką ma wykonywać dana
osoba nie ma potrzeby dodawania żadnych notatek. Notatki należy dodawać tylko
do raportów w których zauważone zostały działania, których dana osoba nie
powinna podejmować - np. surfowanie po internecie, korzystanie z serwisów
społecznościowych lub programów (np. gier), których dany pracownik nie powinien
używać.

Aby dodać nową notatkę do raportu należy kliknąć link "Nowa notatka". Pojawi
się wtedy forma dodawania notatki, gdzie będziemy mogli podać jej **tytuł**
i **treść**. Po kliknięciu "Zapisz" okno zostanie zamknięte a nowa notatka
pojawi się obok zrzutu ekranu raportu. W każdej chwili można edytować każdą
notatkę dodając do niej kolejne informacje.

Zestawienie notatek
-------------------

W zestawieniu notatek są wyświetlane wszystkie notatki, które zostały dodane
do raportów dla konkretnego komputera.

Aby zobaczyć zestawienie notatek należy wejść do listy komputerów i kliknąć
"Zestawienie notatek" dla konkretnego komputera. Domyślnie są wyświetlane
notatki za okres ostatniego miesiąca. Można zmienić zakres dat i kliknąć
"Szukaj" - wtedy zostaną wyświetlone notatki utworzone w zadanym okresie.

Każdą notatkę można edytować lub usunąć.

Po kliknięciu "Pokaż notatkę" zostanie wyświetlona pełna treść notatki wraz
z danymi z raportu oraz ważnymi informacjami o komputerze dla którego raport
został wysłany.

Na żywo
-------

Tryb "Na żywo" służy do obserwowania najnowszych zrzutów ekranu wysyłanych
przez aplikację klienta dla danego komputera. Jego funkcjonowanie zależy
od tego jak często aplikacja klienta wysyła raporty. Można go wykorzystać
do obserwowania działań pracownika w trybie prawie rzeczywistym. Aby osiągnąć
taki efekt należy zmniejszyć czas opóźnienia w aplikacji klienta. Im mniejszy
czas opóźnienia, tym częściej aplikacja klienta będzie wysyłać raporty.
Od wersji 1.1.x istnieje możliwość szybkiej zmiany czasu opóźnienia
dla aplikacji monitorującej dany komputer w ustawieniach danego komputera
po stronie aplikacji serwerowej.

Efektem ubocznym jest duży wzrost liczby raportów od obserwowanego użytkownika.
Z trybu najlepiej jest korzystać w porozumieniu z administratorem, który
na czas obserwacji zmniejszy dla danej aplikacji czas opóźnienia a później
przywróci jego domyślną wartość.

Aby przejść w omawiany tryb należy wejść do listy komputerów i kliknąć
"Na żywo" dla konkretnego komputera.

Tryb "Na żywo" odświeża zrzut ekranu z ostatniego raportu co 10 sekund, więc
nie ma sensu ustawianie mniejszego opóźnienia w aplikacji klienta.

Lista moich notatek
-------------------

Na liście moich notatek użytkownik o uprawnieniach obserwatora zobaczy
wszystkie istniejące w systemie notatki jakie utworzył. Użytkownik o
uprawnieniach administratora widzi na tej liście wszystkie notatki
znajdujące się w systemie.

Jest możliwość wyszukiwania notatek po słowach i zwrotach zawartych w tytule
lub treści lub wyszukania notatek dla konkretnego komputera.

Dla każdej notatki istnieje możliwość edycji, usuwania i pokazywania pełnej
treści wraz z dodatkowymi danymi.

Opis aplikacji klienta
======================

Aplikacja klienta jest instalowana w katalogu
"Program Files (x86)\\WorkWatcherClient" na 64-bitowych systemach lub
"Program Files\\WorkWatcherClient" na 32-bitowych systemach.

W czasie instalacji aplikacja zostaje dodana do rejestru aby była uruchamiana
w trakcie startu systemu. Po zainstalowaniu aplikacji klienta należy
przeprowadzić jej konfigurację a następnie ponownie uruchomić system.

Aby wyłączyć lub włączyć uruchamianie aplikacji klienta w czasie startu systemu
należy uruchomić program msconfig i następnie w zakładce "Uruchamianie"
odznaczyć lub zaznaczyć "WorkWatcherClient".

Aplikacja klienta jest uruchamiana po wykonaniu pliku **ww.exe**. Konfiguracja
jest wczytywana z pliku **ww.ini**.

Plik konfiguracyjny **ww.ini** może wyglądać następująco:

::

  [client]
  delay="600"
  id="535d615cc1049c842f8b45d0"
  key="91d1975ebcd98581c1b1d4f97078e9c5"
  reporting_starts_at="8:00"
  reporting_ends_at="16:00"
  [server]
  host="http://192.168.56.101"

Sekcja [server] określa konfigurację serwera centralnego na którym zbierane są dane
-----------------------------------------------------------------------------------

Parametr **host** jest wymagany i określa adres pod którym znajduje się
aplikacja serwerowa.

Sekcja [client] określa konfigurację aplikacji klienta
------------------------------------------------------

Parametr **delay** jest wymagany i określa co ile sekund aplikacja klienta
będzie wysyłać raport do aplikacji serwerowej.

Parametr **id** jest wymagany i określa **id komputera**, które można znaleźć
na "Liście komputerów" lub "Liście komputerów do wydruku" w aplikacji
serwerowej.

Parametr **key** jest wymagany i określa **klucz** jaki będzie używany przez
aplikację klienta przy dostępie do API aplikacji serwerowej. Wartość tego
parametru dla konkretnego komputera można znaleźć na "Liście komputerów" lub
"Liście komputerów do wydruku" w aplikacji serwerowej.

Dwa powyższe parametry można potraktować trochę jako nazwę użytkownika i hasło,
które służą do identyfikacji i autoryzacji aplikacji klienta w API aplikacji
serwerowej. Parametr id jest zawsze unikatowy, parametr key może być taki sam
dla wszystkich komputerów w sieci.

Parametr **reporting_starts_at** jest opcjonalny i określa godzinę i minutę
od której aplikacja klienta ma zacząć wysyłać raporty.

Parametr **reporting_ends_at** jest opcjonalny i określa godzinę i minutę
od której aplikacja klienta ma zakończyć wysyłanie raportów.

Logi aplikacji klienta
----------------------

Aplikacja klienta zapisuje logi w pliku **ww.log** znajdującym się w katalogu
"Users\\{{nazwa użytkownika}}\\AppData\\Local\\Temp"

Wsparcie techniczne
===================

Wsparcie techniczne można uzyskać pod adresem support@workwatcher.pl
