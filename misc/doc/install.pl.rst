Ta instrukcja jest przeznaczona dla osób, które nie chcą używać referencyjnego
systemu operacyjnego opartego o dystrybucję CentOS i udostępnionego przez firmę
Event Horizon razem z systemem WorkWatcher.

Instalacja systemu WorkWatcher jest wspierana na systemach będących pochodnymi
Red Hat Enterprise Linux i każdy administrator przy minimalnym nakładzie pracy
jest w stanie przeprowadzić instalację takiego systemu zgodnie z instrukcjami,
które znajdują się w pliku misc/bin/reference_server_install_el6.sh w archiwum
z kodem źródłowym projektu.

Dzięki informacją znajdującym się w tym pliku administrator powinien być w
stanie zainstalować system WorkWatcher na innych dystrybucjach Linuksa.

Instalacja części serwerowej WorkWatcher
=========================================

Instalacje systemu na serwerze można podzielić na dwa etapy:

- przygotowanie systemu operacyjnego i oprogramowania

- instalację systemu WorkWatcher

System operacyjny
-----------------

WorkWatcher działa na systemie CentOS 6.x z zainstalowanym PHP 5.4.x oraz
MongoDB 2.4.x. Przed instalacją systemu zaleca się wyłączenie SELinux lub
przełączenie go w tryb permissive.

Instrukcje instalacji poszczególnych pakietów można znaleźć w pliku
misc/bin/reference_server_install_el6.sh w archiwum z kodem źródłowym projektu.

Wykonanie tego skryptu jest zalecane tylko i wyłącznie na nowo zainstalowanym
systemie. Nie zaleca się automatycznej instalacji na działającym systemie na
którym są uruchomione inne usługi. Administrator powinien przejrzeć zawartość
skryptu i dostosować instalację do potrzeb istniejącego systemu.

Instalacja systemu WorkWatcher
-------------------------------

Archiwum z kodem źródłowym należy rozpakować do katalogu /var/www/work-watcher
(lub innego, który będzie skonfigurowany w virtual host).

Do pliku konfiguracyjnego serwera httpd należy dodać

::

  <VirtualHost *:80>
    DocumentRoot /var/www/work-watcher/web
    DirectoryIndex app.php
    <Directory /var/www/work-watcher/web>
      AllowOverride All
      Order allow,deny
      Allow from All
    </Directory>
  </VirtualHost>

W przypadku używania serwera httpd w wersji 2.4 należy wprowadzić zmiany
wkonfiguracji, które szczegółowo zostały opisane w
http://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html .

Istnieje możliwość używania serwera Nginx jak zostało opisane w powyżej
wspomnianej instrukcji, jednak należy pamiętać, że konfiguracja serwera powinna
domyślnie wykonywać plik index.php zamiast app.php. Jest to związane z tym, że
index.php jest linkiem, który w momencie blokowania usługi prowadzi do
statycznej strony html.

Po skopiowaniu kodu źródłowego do katalogu należy do niego wejść z poziomu linii
poleceń

::

  cd /var/www/work-watcher

i wykonać skrypt instalacyjny

::

  sh misc/bin/prod_install.sh

W razie pojawienia się jakichkolwiek problemów na tym etapie instalacji zaleca
się prześledzenie krok po kroku wykonywanych przez skrypt czynności i ustalenie
problematycznego fragmentu.

Jeśli problem jest związany z listami kontroli dostępu, to w
http://symfony.com/doc/current/book/installation.html można znaleźć rozwiązania
tego problemu dla różnych systemów.

Należy zadbać o to, żeby skrypt

::

  php app/check.php

nie wskazywał żadnych problemów.

Po pomyślnie zakończonej instalacji zaleca się uruchomienie zestawu testów. Na
czas uruchomienia testów należy zmienić wartość "development_fixtures" w pliku
app/config/parameters.yml na "true". Po zakończeniu testów wartość tę należy
zmienić na "false". Testy należy uruchomić poleceniem

::

  sh misc/bin/run_phpunit_tests.sh
