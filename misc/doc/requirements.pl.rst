Program klienta
===============

Program klienta można uruchomić na komputerach klasy PC działających pod
kontrolą systemów Windows XP SP3, Windows Vista SP2, Windows 7, Windows 8.

Część serwerowa
===============

Część serwerowa działa na 64 bitowym systemie Enterprise Linux 6.x (RHEL,
CentOS, Scientific Linux, Oracle). Referencyjny system wirtualny musi być
uruchomiony na 64 bitowym systemie operacyjnym.

Wymagania sprzętowe są uzależnione od ilości klientów. Dla referencyjnego
systemu wyglądają następująco:

- do 10 klientów - 1 wirtualny CPU 2.0 GHz, 2 GB RAM
- od 11 do 40 - 1 wirtualny CPU 2.0 GHz, 4 GB RAM
- od 41 do 80 - 2 wirtualne CPU 2.0 GHz, 8 GB RAM
- od 81 do 120 - 3 wirtualne CPU 2.0 GHz, 12 GB RAM

Dla systemów w których ma działać większa ilość klientów zaleca się
zastosowanie dedykowanej maszyny.
