P: Czy dane są bezpieczne?
``````````````````````````

O: System WorkWatcher działa w architekturze klient-serwer. Oprogramowanie
klienckie oraz serwerowe jest instalowane na komputerach firmy, która kupiła
licencję. Żadne dane nie są wysyłane na zewnątrz. Oprogramowanie jest
dostarczane razem z kodem źródłowym, więc każdy może sprawdzić jego działanie.

P: Czy można użyć systemu do monitorowania ludzi pracujących zdalnie?
`````````````````````````````````````````````````````````````````````

O: WorkWatcher może być używany do monitorowania pracowników zdalnych.
Jednak takie monitorowanie musi być oparte o bezpieczne połączenia
http://pl.wikipedia.org/wiki/Virtual_Private_Network .

P: Co się dzieje w czasie awarii sieci?
```````````````````````````````````````

O: Jeśli nastąpi tymczasowa awaria sieci i aplikacja klienta nie może się
skomunikować z aplikacją pracującą na serwerze, to raport zostanie
zbuforowany. Przy następnej próbie wysłania raportu stary raport zostanie
wysłany razem z nowym.
