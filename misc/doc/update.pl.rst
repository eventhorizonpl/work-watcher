Aktualizacja oprogramowania serwerowego
---------------------------------------

Pierwszym krokiem jest skopiowanie na maszynę wirtualną archiwum z najnowszą
wersją programu. Najlepiej jest zrobić to za pomocą programu WinSCP
http://winscp.net/ . Na potrzeby naszego przykładu zakładamy, że archiwum
nazywa się work-watcher-1.0.5.tar.bz2 i kopiujemy go do katalogu /root/ .

Przy logowaniu się do systemu VM hasło dla użytkownika "root" to "workwatcher"
- zalecamy zmianę tego hasła przy pomocy polecenia passwd.

Po skopiowaniu archiwum na VM należy się zalogować przez SSH. Najlepiej jest
zrobić to za pomocą programu PuTTY http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html .

Na początku upewniamy się, że jesteśmy w katalogu w którym jest
zainstalowany WorkWatcher

::

  cd /var/www/work-watcher/

Następnie wyłączamy aplikację serwerową.

::

  sh misc/bin/prod_stop.sh

Po tej czynności w momencie wejścia na adres pod którym jest dostępna aplikacja
serwerowa (standardowo http://192.168.56.101/), powinniśmy zobaczyć komunikat
"Aktualizacja w toku...".

W tym momencie wszystkie aplikacje klienta, które by chciały wysłać dane na
serwer nie mogą tego zrobić i gromadzą raporty w celu późniejszego ich wysłania.

Następnie kopiujemy nową wersję kodu źródłowego

::

  sh misc/bin/update_source_code.sh /root/work-watcher-1.0.5.tar.bz2

i uruchamiamy skrypt aktualizujący.

::

  sh misc/bin/prod_update.sh

Po wykonaniu tej czynności część serwerowa jest już zaktualizowana i gotowa
do pracy.
