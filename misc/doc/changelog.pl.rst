Wersja v1.1
-------------

Aplikacja klienta:
- obsługa API v1.1

Aplikacja serwerowa:

- API v1.1
- nadpisywanie konfiguracji klienta
- zaktualizowano biblioteki frameworka
- udoskonalono interfejs użytkownika
- usprawnienia w działaniu aplikacji


Wersja v1.0.5
-------------

Aplikacja serwerowa:

- udoskonalono skrypt instalacyjny
- udoskonalono skrypt do aktualizacji kodu
- zaktualizowano biblioteki frameworka
- udoskonalono interfejs użytkownika
- usprawnienia w działaniu aplikacji


Wersja v1.0.4
-------------

Aplikacja serwerowa:

- udoskonalono skrypt instalacyjny
- dodano skrypt instalacyjny referencyjnego serwera RHEL7
- zaktualizowano biblioteki frameworka
- udoskonalono interfejs użytkownika
- usprawnienia w działaniu aplikacji


Wersja v1.0.3
-------------

Aplikacja serwerowa:

- tryb "Na żywo"
- udoskonalono cacheowanie obrazków z raportów
- zaktualizowano biblioteki frameworka
- udoskonalono dokumentację
- udoskonalono interfejs użytkownika
- usprawnienia w działaniu aplikacji


Wersja v1.0.2
-------------

Aplikacja klienta:

- dodano informację o wersji klienta i API

Aplikacja serwerowa:

- dodano statystykę informującą o ilości ostrzeżeń na komputer
- dodano parametr umożliwiający dostosowanie opóźnienia przed usunięciem raportu dla którego nie utworzono notatki
- udoskonalono dokumentację
- zaktualizowano biblioteki frameworka
- udoskonalono API do komunikacji z aplikacją klienta
- usprawnienia w działaniu aplikacji


Wersja v1.0.1
-------------

Aplikacja klienta:

- poprawiono komunikat o błędzie w razie nieznalezienia screenshot-cmd.exe

Aplikacja serwerowa:

- dodatkowe możliwości sortowania list grup, komputerów, notatek i użytkowników
- udoskonalono dokumentację
- udoskonalono interfejs użytkownika
- zaktualizowano biblioteki frameworka
- usprawnienia w działaniu aplikacji

Referencyjny system serwerowy:

- synchronizacja czasu z zegarem atomowym przez ntpdate
