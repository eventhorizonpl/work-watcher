#!/bin/sh

HOSTNAME=workwatcher

echo "127.0.0.1 $HOSTNAME" >> /etc/hosts

setenforce 0
sed -i "s@^\(SELINUX=\).*@\1disabled@" /etc/selinux/config

chkconfig iptables off
chkconfig ip6tables off
chkconfig netfs off

rpm -ihv http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ihv http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

mount -t tmpfs -o size=512m tmpfs /var/cache/yum

yum install --assumeyes --enablerepo=remi-php55 --enablerepo=remi httpd mc mongodb mongodb-server ntpdate \
	php php-cli php-common php-gd php-intl php-mbstring php-pdo php-pecl-apcu php-pecl-mongo php-process php-snmp rsync

echo 0.fedora.pool.ntp.org >> /etc/ntp/step-tickers

sed -i "s/;date.timezone =/date.timezone = \"Europe\/Warsaw\"/g" /etc/php.ini
sed -i "s/memory_limit = 128M/memory_limit = 1024M/g" /etc/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 8M/g" /etc/php.ini

echo xdebug.max_nesting_level=200 >> /etc/php.d/xdebug.ini

cat >> /etc/httpd/conf/httpd.conf << "EOF"
<VirtualHost *:80>
    DocumentRoot /var/www/work-watcher/web
    DirectoryIndex app.php
    <Directory /var/www/work-watcher/web>
        AllowOverride All
        Order allow,deny
        Allow from All
    </Directory>
</VirtualHost>
EOF

chkconfig httpd on
chkconfig mongod on
chkconfig ntpdate on

/etc/init.d/httpd restart
/etc/init.d/mongod restart
/etc/init.d/ntpdate restart

#link cron script
