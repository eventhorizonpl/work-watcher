#!/bin/sh

rm -rf app/cache/*

rm -rf web/bundles/*
rm -rf web/css/*
rm -rf web/js/*

php app/console assets:install web --symlink --relative
php app/console assetic:dump
