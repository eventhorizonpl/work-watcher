#!/bin/sh

if [ ! -e phpDocumentor.phar ]
then
    sh misc/bin/get_phpDocumentor.sh
fi

php phpDocumentor.phar -d ./src -t ./misc/phpdoc
