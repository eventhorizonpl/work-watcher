#!/bin/sh

VERSION=`cat app/config/parameters_constant.yml | grep version | cut -d ":" -f 2 | cut -d "v" -f 2`
PROJECT_DIR="work-watcher"
BACKUP_TAR=$PROJECT_DIR"-"$VERSION".tar"

rm -rf app/cache/*
rm -rf app/logs/*
rm -rf misc/client/dist/
rm -rf misc/client/__pycache__/
rm -rf misc/client/screenshot-cmd.exe
rm -rf misc/doc/*.pdf

cd ..

tar -cvf $BACKUP_TAR -X $PROJECT_DIR"/misc/data/tar_exclude.txt" $PROJECT_DIR
bzip2 -9 $BACKUP_TAR
md5sum $BACKUP_TAR.bz2 > $BACKUP_TAR.bz2.md5sum
sha1sum $BACKUP_TAR.bz2 > $BACKUP_TAR.bz2.sha1sum
sha512sum $BACKUP_TAR.bz2 > $BACKUP_TAR.bz2.sha512sum
