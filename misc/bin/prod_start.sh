#!/bin/sh

if [ -f web/index.php ]
then
    rm web/index.php
fi

ln -s app.php web/index.php

rm -rf app/cache/*
