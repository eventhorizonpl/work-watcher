#/bin/sh

cd /var/www/work-watcher

if [ "$1" == "dev" ]
then
    php app/console workwatcher:delete-old-reports --env=dev
else
    php app/console workwatcher:delete-old-reports --env=prod
fi
