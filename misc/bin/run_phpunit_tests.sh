#!/bin/sh

rm -rf app/cache/*

sh misc/bin/regenerate_db.sh test

COMMAND="bin/phpunit -c app --verbose"

for i in "$@"
do
    case $i in
        ch|--coverage-html)
            COMMAND=$COMMAND" --coverage-html ../tmp/report"
            shift
            ;;
        pi|--process-isolation)
            COMMAND=$COMMAND" --process-isolation"
            shift
            ;;
        *)
            echo "unknown option"
            ;;
    esac
    shift
done

echo "COMMAND="$COMMAND

$COMMAND
