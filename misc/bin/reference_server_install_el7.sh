#!/bin/sh

HOSTNAME=workwatcher

echo $HOSTNAME > /etc/hostname
echo "127.0.0.1 $HOSTNAME" >> /etc/hosts

setenforce 0
sed -i "s@^\(SELINUX=\).*@\1disabled@" /etc/selinux/config

rpm -ihv http://dl.fedoraproject.org/pub/epel/beta/7/x86_64/epel-release-7-0.2.noarch.rpm

mount -t tmpfs -o size=512m tmpfs /var/cache/yum

yum install --assumeyes bzip2 httpd mc mongodb mongodb-server ntpdate \
    php php-cli php-common php-gd php-intl php-mbstring php-pdo php-pecl-apcu php-pecl-mongo php-process php-snmp \
    postfix rsync

sed -i "s/;date.timezone =/date.timezone = \"Europe\/Warsaw\"/g" /etc/php.ini
sed -i "s/memory_limit = 128M/memory_limit = 1024M/g" /etc/php.ini
sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 8M/g" /etc/php.ini

echo xdebug.max_nesting_level=200 >> /etc/php.d/xdebug.ini

cat > /etc/httpd/conf.d/virtual.conf << "EOF"
<VirtualHost *:80>
    DocumentRoot /var/www/work-watcher/web
    DirectoryIndex app.php
    <Directory /var/www/work-watcher/web>
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
EOF

firewall-cmd --permanent --zone=public --add-service=http

systemctl enable httpd.service
systemctl enable mongod.service
systemctl enable ntpdate.service
systemctl enable postfix.service

systemctl restart firewalld.service
systemctl restart httpd.service
systemctl restart mongod.service
systemctl restart ntpdate.service
systemctl restart postfix.service

#link cron script
