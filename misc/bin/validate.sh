#!/bin/sh

php app/console doctrine:mongodb:mapping:info --verbose
php app/console twig:lint src/
pylint --max-line-length=160 misc/client/ww.py
