#!/bin/sh

rm -rf app/cache/*

sh misc/bin/regenerate_acl.sh

if [ ! -e composer.phar ]
then
    sh misc/bin/get_composer.sh
fi

php composer.phar --optimize-autoloader install

sh misc/bin/regenerate_db.sh dev

sh misc/bin/regenerate_assets.sh

php app/console cache:warmup --env=dev

php app/check.php
