#!/bin/sh

if [[ -z "$1" ]]
then
    ENV="dev"
else
    ENV=$1
fi

echo "ENV=$ENV"

php app/console doctrine:mongodb:schema:drop --env=$ENV
php app/console doctrine:mongodb:schema:create --env=$ENV
php app/console doctrine:mongodb:fixtures:load --no-interaction --env=$ENV
#php app/console doctrine:mongodb:schema:update --env=$ENV
