#!/bin/sh

rm -rf app/cache/*

if [ ! -e composer.phar ]
then
    sh misc/bin/get_composer.sh
fi

php composer.phar --optimize-autoloader install

php app/console doctrine:mongodb:schema:update --env=dev

sh misc/bin/regenerate_assets.sh

php app/console workwatcher:regenerate-pages --env=dev

php app/console cache:warmup --env=dev

php app/check.php
