#!/bin/sh

TMP_DIR="/tmp/"
WW_TMP_DIR=$TMP_DIR"/work-watcher"
WW_DIR="/var/www/"

if [[ ! -d $WW_TMP_DIR ]]
then
    echo "Creating temp dir..."
    mkdir $WW_TMP_DIR
fi

mount -t tmpfs -o size=512m tmpfs $WW_TMP_DIR

if [[ ! -z "$1" ]]
then
    FILE=$1
    if [[ ! -f $FILE ]]
    then
        exit 1
    fi
else
    exit 1
fi

echo "Unpacking to temp dir..."
tar xjf $FILE -C $TMP_DIR

echo "Updating source code..."
rsync -av --force --delete --progress --exclude-from=misc/data/rsync_exclude.txt $WW_TMP_DIR $WW_DIR

umount $WW_TMP_DIR

if [[ -d $WW_TMP_DIR ]]
then
    echo "Removing temp dir..."
    rm -rf $WW_TMP_DIR
fi
