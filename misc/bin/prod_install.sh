#!/bin/sh

sh misc/bin/prod_stop.sh

rm -rf app/cache/*

sh misc/bin/regenerate_acl.sh

if [ ! -e composer.phar ]
then
    sh misc/bin/get_composer.sh
fi

php composer.phar --optimize-autoloader install

sh misc/bin/regenerate_db.sh prod

sh misc/bin/regenerate_assets.sh

sh misc/bin/prod_start.sh

php app/console cache:warmup --env=prod

php app/check.php
