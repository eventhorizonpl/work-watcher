#!/bin/sh

sh misc/bin/prod_stop.sh

rm -rf app/cache/*

if [ ! -e composer.phar ]
then
    sh misc/bin/get_composer.sh
fi

php composer.phar --optimize-autoloader install

php app/console doctrine:mongodb:schema:update --env=prod

sh misc/bin/regenerate_assets.sh

php app/console workwatcher:regenerate-pages --env=prod

sh misc/bin/prod_start.sh

php app/console cache:warmup --env=prod

php app/check.php
