OutFile "setup.exe"

InstallDir $PROGRAMFILES\WorkWatcherClient

Section "Installer Section"
    SetOutPath $INSTDIR

    File _bz2.pyd
    File _ctypes.pyd
    File _hashlib.pyd
    File _lzma.pyd
    File pyexpat.pyd
    File python33.dll
    File screenshot-cmd.exe
    File select.pyd
    File _socket.pyd
    File _ssl.pyd
    File unicodedata.pyd
    File ww.exe
    File ww.ini

    FileOpen $9 $INSTDIR\ww.vbs w
    FileWrite $9 'Dim WinScriptHost$\r$\n'
    FileWrite $9 'Set WinScriptHost = CreateObject("WScript.Shell")$\r$\n'
    FileWrite $9 'WinScriptHost.Run Chr(34) & "$INSTDIR\ww.exe" & Chr(34), 0$\r$\n'
    FileWrite $9 'Set WinScriptHost = Nothing$\r$\n'
    FileClose $9 ;

    WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "WorkWatcherClient" '$INSTDIR\ww.vbs'

    WriteUninstaller $INSTDIR\uninstaller.exe
SectionEnd

Section "un.Uninstall"
    Delete $INSTDIR\uninstaller.exe
    Delete $INSTDIR\_bz2.pyd
    Delete $INSTDIR\_ctypes.pyd
    Delete $INSTDIR\_hashlib.pyd
    Delete $INSTDIR\_lzma.pyd
    Delete $INSTDIR\pyexpat.pyd
    Delete $INSTDIR\python33.dll
    Delete $INSTDIR\screenshot-cmd.exe
    Delete $INSTDIR\select.pyd
    Delete $INSTDIR\_socket.pyd
    Delete $INSTDIR\_ssl.pyd
    Delete $INSTDIR\unicodedata.pyd
    Delete $INSTDIR\ww.exe
    Delete $INSTDIR\ww.ini
    Delete $INSTDIR\ww.vbs
    RMDir $INSTDIR

    DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Run" "WorkWatcherClient"
SectionEnd
