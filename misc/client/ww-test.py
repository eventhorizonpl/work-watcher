import configparser, os, re, sys, unittest

from ww import WorkWatcherClient

class WorkWatcherClientTest(unittest.TestCase):
    def test_check_time1(self):
        client = WorkWatcherClient()
        client.reporting_starts_at = "00:00"
        client.reporting_ends_at = "23:59"
        result = client.check_time()
        self.assertTrue(result)

    def test_check_time2(self):
        client = WorkWatcherClient()
        client.reporting_starts_at = "00:00"
        client.reporting_ends_at = "00:00"
        result = client.check_time()
        self.assertFalse(result)

    def test_create_report1(self):
        client = WorkWatcherClient()
        self.assertEqual({}, client.reports)

    def test_create_report2(self):
        client = WorkWatcherClient()
        result = client.create_report()
        self.assertIsNotNone(client.reports)

    def test_create_report3(self):
        client = WorkWatcherClient()
        self.assertIsNone(client.create_report())

    def test_log(self):
        client = WorkWatcherClient()
        self.assertIsNone(client.log("test message"))

    def test_read_config_file1(self):
        client = WorkWatcherClient()
        self.assertIsNone(client.read_config_file())

    def test_read_config_file2(self):
        client = WorkWatcherClient()
        client.read_config_file()
        self.assertEqual(10, client.client_delay)

    def test_read_config_file3(self):
        client = WorkWatcherClient()
        client.read_config_file()
        config_file = 'ww.ini'
        config = configparser.ConfigParser()
        config.read(config_file)
        self.assertEqual(re.sub('"', '', config['client']['id']), client.computer_id)

    def test_read_config_file4(self):
        client = WorkWatcherClient()
        client.read_config_file()
        config_file = 'ww.ini'
        config = configparser.ConfigParser()
        config.read(config_file)
        self.assertEqual(re.sub('"', '', config['client']['key']), client.computer_key)

    def test_read_config_file5(self):
        client = WorkWatcherClient()
        client.read_config_file()
        self.assertIsNone(client.reporting_ends_at)

    def test_read_config_file6(self):
        client = WorkWatcherClient()
        client.read_config_file()
        self.assertIsNone(client.reporting_starts_at)

    def test_read_config_file7(self):
        client = WorkWatcherClient()
        client.read_config_file()
        self.assertEqual("http://ww.local/app_dev.php/", client.server_host)

    def test_send_10_reports(self):
        client = WorkWatcherClient()
        client.read_config_file()
        client.create_report()
        self.assertIsNone(client.send_10_reports())

    def test_send_reports(self):
        client = WorkWatcherClient()
        client.read_config_file()
        client.create_report()
        self.assertIsNone(client.send_reports())

    def test_test_api1(self):
        client = WorkWatcherClient()
        client.read_config_file()
        result = client.test_api("v1.0")
        self.assertTrue(result)

    def test_test_api2(self):
        client = WorkWatcherClient()
        client.read_config_file()
        result = client.test_api("v2.0")
        self.assertFalse(result)

if __name__ == '__main__':
    unittest.main()
