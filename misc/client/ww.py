"""
    WorkWatcher client
"""
import argparse, configparser, datetime, getpass, json, os, re, requests, socket, subprocess, sys, tempfile, time

class WorkWatcherClient(object):
    """
        Main WorkWatcher client class
    """
    client_delay = None
    computer_id = None
    computer_key = None
    configuration = None
    reporting_ends_at = None
    reporting_starts_at = None
    reports = {}
    server_host = None

    def check_time(self):
        """
            Checks if client can be run.
            Returns:
                boolean
        """
        current_time = int(time.time())
        try:
            starts_at = int(datetime.datetime.strptime(str(time.strftime("%Y-%m-%d") + " " + self.reporting_starts_at), "%Y-%m-%d %H:%M").timestamp())
        except ValueError:
            self.log("ERROR: Wrong reporting_starts_at value in config file.")
            sys.exit(1)
        try:
            ends_at = int(datetime.datetime.strptime(str(time.strftime("%Y-%m-%d") + " " + self.reporting_ends_at), "%Y-%m-%d %H:%M").timestamp())
        except ValueError:
            self.log("ERROR: Wrong reporting_ends_at value in config file.")
            sys.exit(1)

        if (current_time > starts_at) and (current_time < ends_at):
            return True
        else:
            return False

    def create_report(self):
        """
            Creates report.
        """
        file_name = str("screen-" + time.strftime("%Y%m%d%H%M%S") + ".png")
        tmp_file = str(tempfile.gettempdir() + os.sep + file_name)
        try:
            subprocess.call("screenshot-cmd.exe -o " + tmp_file)
        except FileNotFoundError:
            self.log("ERROR: screenshot-cmd.exe not found?")
            sys.exit(1)
        self.reports[time.strftime("%Y%m%d%H%M%S")] = {
            'date': time.strftime("%Y-%m-%d"),
            'file': tmp_file,
            'file_name': file_name,
            'hostname': socket.gethostname(),
            'time': time.strftime("%H:%M:%S"),
            'username': getpass.getuser()
        }

    def log(self, message):
        """
            Logs message.
            Args:
                message: message string
        """
        print(message)
        message = str(time.strftime("%Y%m%d%H%M%S") + " " + message + "\n")
        tmp_file = open(str(tempfile.gettempdir() + os.sep + "ww.log"), 'a')
        tmp_file.write(message)
        tmp_file.close()

    def process(self):
        """
            Main process.
        """
        self.create_report()
        self.send_reports()

        time.sleep(self.client_delay)

    def read_config_file(self):
        """
            Reads config file.
        """
        remove_chars = '"'
        if getattr(sys, 'frozen', False):
            config_file = os.path.dirname(sys.executable) + os.sep + 'ww.ini'
        else:
            config_file = os.path.abspath(os.path.dirname(__file__)) + os.sep + 'ww.ini'
        try:
            config = configparser.ConfigParser()
            config.read(config_file)
            try:
                if 'delay' in config['client']:
                    self.client_delay = float(re.sub(remove_chars, '', config['client']['delay']))
                else:
                    self.log("ERROR: No delay in config file.")
                    sys.exit(1)
            except ValueError:
                self.log("ERROR: Wrong delay value in config file.")
                sys.exit(1)
            if 'id' in config['client']:
                self.computer_id = re.sub(remove_chars, '', config['client']['id'])
            else:
                self.log("ERROR: No computer id in config file.")
                sys.exit(1)
            if 'key' in config['client']:
                self.computer_key = re.sub(remove_chars, '', config['client']['key'])
            else:
                self.log("ERROR: No computer key in config file.")
                sys.exit(1)
            if 'reporting_ends_at' in config['client']:
                self.reporting_ends_at = re.sub(remove_chars, '', config['client']['reporting_ends_at'])
            if 'reporting_starts_at' in config['client']:
                self.reporting_starts_at = re.sub(remove_chars, '', config['client']['reporting_starts_at'])
            if 'host' in config['server']:
                self.server_host = re.sub(remove_chars, '', config['server']['host'])
            else:
                self.log("ERROR: No server address in config file.")
                sys.exit(1)
        except configparser.ParsingError:
            self.log("ERROR: Wrong config file format.")
            sys.exit(1)

    def read_11_configuration_from_server(self):
        """
            Read configuration from API v1.1 server.
        """

        url = str(self.server_host.rstrip("/") + "/api/v1.1/computers/" + self.computer_id)

        response = requests.get(url)

        if (response.status_code == 200):
            decoded_response = json.loads(response.text)
            if (('data' in decoded_response) and ('overwrite_client_configuration' in decoded_response['data']) and (decoded_response['data']['overwrite_client_configuration'] == True)):
                if ('client_delay' in decoded_response['data']):
                    self.client_delay = decoded_response['data']['client_delay']
        else:
            self.log(str("error " + str(response.status_code) + " while reading configuration from server"))

    def send_10_reports(self):
        """
            Send API v1.0 reports.
        """
        keys = []
        for report_key, report_value in self.reports.items():
            url = str(self.server_host.rstrip("/") + "/api/v1.0/report/submit")

            data = {
                'computer_id': self.computer_id,
                'computer_key': self.computer_key,
                'date': report_value['date'],
                'hostname': report_value['hostname'],
                'time': report_value['time'],
                'username': report_value['username']
            }

            try:
                tmp_file = open(report_value['file'], 'rb')
                files = {'file': (report_value['file_name'], tmp_file, 'image/png', {'Expires': '0'})}
                response = requests.post(url, data=data, files=files)
                tmp_file.close()

                if (response.status_code == 200) and (response.text == '{"status":"ok"}'):
                    try:
                        os.remove(report_value['file'])
                    except PermissionError:
                        self.log(str("ERROR: Cannot delete file " + report_value['file']))
                    except FileNotFoundError:
                        self.log(str("ERROR: File not found for remove " + report_value['file']))
                    keys.append(report_key)
                else:
                    self.log(str("error " + str(response.status_code) + " while sending " + report_value['file'] + " to " + url))
            except FileNotFoundError:
                self.log(str("ERROR: File not found for open " + report_value['file']))

        for key in keys:
            self.reports.pop(key, None)

    def send_11_reports(self):
        """
            Send API v1.1 reports.
        """
        keys = []
        for report_key, report_value in self.reports.items():
            url = str(self.server_host.rstrip("/") + "/api/v1.1/reports")

            data = {
                'computer_id': self.computer_id,
                'computer_key': self.computer_key,
                'date': report_value['date'],
                'hostname': report_value['hostname'],
                'time': report_value['time'],
                'username': report_value['username']
            }

            try:
                tmp_file = open(report_value['file'], 'rb')
                files = {'file': (report_value['file_name'], tmp_file, 'image/png', {'Expires': '0'})}
                response = requests.post(url, data=data, files=files)
                tmp_file.close()

                if (response.status_code == 201):
                    try:
                        os.remove(report_value['file'])
                    except PermissionError:
                        self.log(str("ERROR: Cannot delete file " + report_value['file']))
                    except FileNotFoundError:
                        self.log(str("ERROR: File not found for remove " + report_value['file']))
                    keys.append(report_key)
                else:
                    self.log(str("error " + str(response.status_code) + " while sending " + report_value['file'] + " to " + url))
            except FileNotFoundError:
                self.log(str("ERROR: File not found for open " + report_value['file']))

        for key in keys:
            self.reports.pop(key, None)

    def send_reports(self):
        """
            Send gathered reports.
        """
        try:
            if (self.test_api("v1.1")):
                self.read_11_configuration_from_server()
                self.send_11_reports()
            elif (self.test_api("v1.0")):
                self.send_10_reports()
            else:
                self.log("ERROR: Cannot reach API 1.0?")
                sys.exit(1)

        except requests.exceptions.ConnectionError:
            self.log("ERROR: Server connection error")
        except requests.exceptions.MissingSchema:
            self.log("ERROR: Wrong server address")
            sys.exit(1)

    def start(self):
        """
            Main program loop.
        """
        self.log("starting client application")

        while True:
            self.read_config_file()
            if self.reporting_starts_at and self.reporting_ends_at:
                if self.check_time():
                    self.process()
                else:
                    time.sleep(self.client_delay)
            else:
                self.process()

    def test_api(self, version):
        """
            Tests API for certain version.
            Args:
                version: API version string
            Returns:
                boolean
        """
        url = str(self.server_host.rstrip("/") + "/api/" + version + "/test")
        response = requests.get(url)
        if (response.status_code == 200):
            return True
        else:
            return False

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WorkWatcher client application.')
    parser.add_argument('--version', action='version', version='WorkWatcher client v1.1 API v1.1')
    args = parser.parse_args()

    client = WorkWatcherClient()
    client.start()
