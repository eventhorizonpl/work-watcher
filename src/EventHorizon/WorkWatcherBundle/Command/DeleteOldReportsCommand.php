<?php

namespace EventHorizon\WorkWatcherBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOldReportsCommand extends ContainerAwareCommand
{
    /**
     * Command configuration.
     */
    protected function configure()
    {
        $this
            ->setName('workwatcher:delete-old-reports')
            ->addOption('days', 'd', InputOption::VALUE_REQUIRED, 'Days')
            ->setDescription('Delete old reports')
        ;
    }

    /**
     * Command execion.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Deleting old reports...");

        if ($input->getOption('days')) {
            $days = (int) $input->getOption('days');
        } else {
            $days = (int) $this->getContainer()->getParameter('reports_delete_delay');
        }

        if ((null === $days) or (0 === $days)) {
            $days = 30;
        }

        $beforeDate = new \DateTime(date("Y-m-d"));
        $beforeDate->modify('-'.$days.' days');
        $beforeDate->setTime(0, 0, 0);

        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, 2);

        $dm = $this->getContainer()->get('doctrine_mongodb');

        $progress->advance();

        $dm->getRepository('EventHorizonWorkWatcherBundle:Report')->removeReportsByDateV1($beforeDate);
        $progress->advance();

        $progress->finish();

        $output->writeln("Done.");
    }
}
