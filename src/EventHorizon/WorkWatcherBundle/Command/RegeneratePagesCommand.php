<?php

namespace EventHorizon\WorkWatcherBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use EventHorizon\PageBundle\Document\Page;

class RegeneratePagesCommand extends ContainerAwareCommand
{
    /**
     * Command configuration.
     */
    protected function configure()
    {
        $this
            ->setName('workwatcher:regenerate-pages')
            ->setDescription('Regenerate pages from fixtures')
        ;
    }

    /**
     * Command execion.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Regenerating pages from fixtures...");

        $dm = $this->getContainer()->get('doctrine_mongodb');
        $manager = $dm->getManager();

        $locale = "pl";
        $name = "help";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/help.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('Pomoc');
        $page->setIsVisible(true);
        $manager->persist($page);

        $locale = "pl";
        $name = "install";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/install.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('Instalacja systemu');
        $page->setIsVisible(true);
        $manager->persist($page);

        $locale = "pl";
        $name = "license";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/license.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('Licencja');
        $page->setIsVisible(true);
        $manager->persist($page);

        $locale = "pl";
        $name = "requirements";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/requirements.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('WorkWatcher - wymagania sprzętowe/systemowe');
        $page->setIsVisible(true);
        $manager->persist($page);

        $locale = "pl";
        $name = "changelog";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/changelog.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('Lista zmian');
        $page->setIsVisible(true);
        $manager->persist($page);

        $locale = "pl";
        $name = "update";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/update.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('Aktualizacja');
        $page->setIsVisible(true);
        $manager->persist($page);

        $locale = "pl";
        $name = "faq";

        $page = $dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);

        if (null === $page) {
            $page = new Page();
        }

        $content = file_get_contents('misc/doc/faq.pl.html');

        $page->setContent($content);
        $page->setName($name);
        $page->setLocale($locale);
        $page->setTitle('Często zadawane pytania');
        $page->setIsVisible(true);
        $manager->persist($page);

        $manager->flush();

        $output->writeln("Done.");
    }
}
