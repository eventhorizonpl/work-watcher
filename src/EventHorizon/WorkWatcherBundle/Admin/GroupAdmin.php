<?php

namespace EventHorizon\WorkWatcherBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class GroupAdmin extends Admin
{
    /**
     * Configures datagrid filters.
     *
     * @param DatagridMapper $datagrid A DatagridMapper instance
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('name')
        ;
    }

    /**
     * Configures form fields.
     *
     * @param FormMapper $formMapper A FormMapper instance
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o grupie'))
                ->add('name', null, array('label' => 'Nazwa'))
                ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_OBSERVER' => 'ROLE_OBSERVER',
                    ),
                    'label' => 'Uprawnienia',
                    'multiple' => true,
                    'required' => false,
                ))
            ->end()
        ;
    }

    /**
     * Configures list fields.
     *
     * @param ListMapper $listMapper A ListMapper instance
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('name')
            ->add('roles')
        ;
    }

    /**
     * Configures routes.
     *
     * @param RouteCollection $collection A RouteCollection instance
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
