<?php

namespace EventHorizon\WorkWatcherBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends Admin
{
    /**
     * Configures datagrid filters.
     *
     * @param DatagridMapper $datagrid A DatagridMapper instance
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('locked')
            ->add('expired')
        ;
    }

    /**
     * Configures form fields.
     *
     * @param FormMapper $formMapper A FormMapper instance
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o użytkowniku'))
                ->add('email', null, array('label' => 'E-mail'))
                ->add('username', null, array('label' => 'Nazwa użytkownika'))
            ->end()
            ->with('Ustawienia konta', array('description' => 'Dodatkowe ustawienia konta'))
                ->add('roles', 'choice', array(
                    'choices' => array(
                        'ROLE_ADMIN' => 'ROLE_ADMIN',
                        'ROLE_OBSERVER' => 'ROLE_OBSERVER',
                    ),
                    'label' => 'Uprawnienia',
                    'multiple' => true,
                    'required' => false,
                ))
                ->add('enabled', null, array(
                    'label' => 'Włączone',
                    'required' => false,
                ))
                ->add('locked', null, array(
                    'label' => 'Zablokowane',
                    'required' => false,
                ))
                ->add('expired', null, array(
                    'label' => 'Zdezaktualizowane',
                    'required' => false,
                ))
            ->end()
        ;
    }

    /**
     * Configures list fields.
     *
     * @param ListMapper $listMapper A ListMapper instance
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('username')
            ->add('email')
            ->add('roles')
            ->add('enabled')
            ->add('locked')
            ->add('expired')
        ;
    }

    /**
     * Configures routes.
     *
     * @param RouteCollection $collection A RouteCollection instance
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
