<?php

namespace EventHorizon\WorkWatcherBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class NoteAdmin extends Admin
{
    /**
     * Configures datagrid filters.
     *
     * @param DatagridMapper $datagrid A DatagridMapper instance
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('computer')
            ->add('report')
            ->add('user')
            ->add('content')
            ->add('title')
        ;
    }

    /**
     * Configures form fields.
     *
     * @param FormMapper $formMapper A FormMapper instance
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o notatce'))
                ->add('content', null, array('label' => 'Treść'))
                ->add('title', null, array('label' => 'Tytuł'))
            ->end()
        ;
    }

    /**
     * Configures list fields.
     *
     * @param ListMapper $listMapper A ListMapper instance
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('computer')
            ->add('report')
            ->add('user')
            ->add('content')
            ->add('title')
        ;
    }

    /**
     * Configures routes.
     *
     * @param RouteCollection $collection A RouteCollection instance
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
