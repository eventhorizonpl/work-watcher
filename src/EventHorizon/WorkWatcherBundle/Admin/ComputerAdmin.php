<?php

namespace EventHorizon\WorkWatcherBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ComputerAdmin extends Admin
{
    /**
     * Configures datagrid filters.
     *
     * @param DatagridMapper $datagrid A DatagridMapper instance
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('group')
            ->add('user')
            ->add('description')
            ->add('key')
            ->add('name')
        ;
    }

    /**
     * Configures form fields.
     *
     * @param FormMapper $formMapper A FormMapper instance
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Ogólne', array('description' => 'Ogólne informacje o komputerze'))
                ->add('description', null, array(
                    'label' => 'Opis',
                    'required' => false,
                ))
                ->add('key', null, array('label' => 'Klucz'))
                ->add('name', null, array('label' => 'Nazwa'))
            ->end()
            ->with('Dodatkowe', array('description' => 'Dodatkowe informacje o komputerze'))
                ->add('group', null, array('label' => 'Grupa'))
            ->end()
        ;
    }

    /**
     * Configures list fields.
     *
     * @param ListMapper $listMapper A ListMapper instance
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('group')
            ->add('user')
            ->add('description')
            ->add('key')
            ->add('name')
        ;
    }

    /**
     * Configures routes.
     *
     * @param RouteCollection $collection A RouteCollection instance
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
}
