<?php

namespace EventHorizon\WorkWatcherBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ReportAdmin extends Admin
{
    /**
     * Configures datagrid filters.
     *
     * @param DatagridMapper $datagrid A DatagridMapper instance
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('id')
            ->add('computer')
            ->add('date')
            ->add('ip')
        ;
    }

    /**
     * Configures list fields.
     *
     * @param ListMapper $listMapper A ListMapper instance
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('computer')
            ->add('date')
            ->add('ip')
        ;
    }

    /**
     * Configures routes.
     *
     * @param RouteCollection $collection A RouteCollection instance
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
        $collection->remove('edit');
    }
}
