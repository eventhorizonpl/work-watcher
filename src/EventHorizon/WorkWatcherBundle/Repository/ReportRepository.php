<?php

namespace EventHorizon\WorkWatcherBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ReportRepository extends DocumentRepository
{
    /**
     * Returns Report document by computer id ordered by createdAt DESC.
     *
     * @param string $computerId A Computer document id
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Report|null A Report instance
     */
    public function getReportByComputerIdV1($computerId)
    {
        return $this->createQueryBuilder()
            ->field('computer.id')->equals($computerId)
            ->sort('createdAt', 'desc')
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Returns Report documents by computer id and date end and date start ordered by date ASC and time ASC.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Computer $computer  A Computer document
     * @param \DateTime                                         $dateEnd   An end date
     * @param \DateTime                                         $dateStart A start date
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function getReportsByComputerIdAndDateEndAndDateStartV1(\EventHorizon\WorkWatcherBundle\Document\Computer $computer, \DateTime $dateEnd, \DateTime $dateStart)
    {
        return $this->createQueryBuilder()
            ->field('computer.id')->equals($computer->getId())
            ->field('createdAt')->gt($dateStart)
            ->field('createdAt')->lt($dateEnd)
            ->sort('date', 'asc')
            ->sort('time', 'asc')
            ->getQuery()
            ->execute();
    }

    /**
     * Returns Report documents count by date end and date start.
     *
     * @param \DateTime|null $dateEnd   An end date
     * @param \DateTime|null $dateStart A start date
     *
     * @return integer
     */
    public function getReportsByDateEndAndDateStartV1($dateEnd, $dateStart)
    {
        $query = $this->createQueryBuilder();

        if ($dateEnd) {
            $query->field('createdAt')->lt($dateEnd);
        }

        if ($dateStart) {
            $query->field('createdAt')->gt($dateStart);
        }

        return $query->getQuery()
            ->execute()
            ->count();
    }

    /**
     * Removes Report documents by computer id.
     *
     * @param string $computerId A Computer document id
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function removeReportsByComputerIdV1($computerId)
    {
        return $this->createQueryBuilder()
            ->remove()
            ->field('computer.id')->equals($computerId)
            ->getQuery()
            ->execute();
    }

    /**
     * Removes Report documents by computer id.
     *
     * @param \DateTime $beforeDate A before date
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function removeReportsByDateV1(\DateTime $beforeDate)
    {
        return $this->createQueryBuilder()
            ->remove()
            ->field('note')->equals(null)
            ->field('createdAt')->lt($beforeDate)
            ->getQuery()
            ->execute();
    }
}
