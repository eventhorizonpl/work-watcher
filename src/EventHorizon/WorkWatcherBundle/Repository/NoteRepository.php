<?php

namespace EventHorizon\WorkWatcherBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class NoteRepository extends DocumentRepository
{
    /**
     * Returns Note documents by computer id and date end and date start ordered by title ASC.
     *
     * @param string    $computerId A Computer document id
     * @param \DateTime $dateEnd    An end date
     * @param \DateTime $dateStart  A start date
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function getNotesByComputerIdAndDateEndAndDateStartV1($computerId, \DateTime $dateEnd, \DateTime $dateStart)
    {
        return $this->createQueryBuilder()
            ->field('computer.id')->equals($computerId)
            ->field('createdAt')->gt($dateStart)
            ->field('createdAt')->lt($dateEnd)
            ->sort('title', 'asc')
            ->getQuery()
            ->execute();
    }

    /**
     * Returns Note documents by computer id and user and search.
     *
     * @param string                                     $computerId A Computer document id
     * @param \EventHorizon\SecurityBundle\Document\User $user       An User instance
     * @param string                                     $search     A search string
     *
     * @return \Doctrine\ODM\MongoDB\Query\Query A Query instance
     */
    public function getNotesByComputerIdAndUserAndSearchV1($computerId, \EventHorizon\SecurityBundle\Document\User $user, $search)
    {
        $query = $this->createQueryBuilder()
            ->field('user.id')->equals($user->getId())
            ->sort('title', 'asc');

        if ($computerId) {
            $query->field('computer.id')->equals($computerId);
        }

        if ($search) {
            $query->addOr($query->expr()->field('content')->equals(new \MongoRegex('/.*'.$search.'.*/')));
            $query->addOr($query->expr()->field('title')->equals(new \MongoRegex('/.*'.$search.'.*/')));
        }

        $query->getQuery();

        return $query;
    }

    /**
     * Returns Note documents by computer id and search.
     *
     * @param string $computerId A Computer document id
     * @param string $search     A search string
     *
     * @return \Doctrine\ODM\MongoDB\Query\Query A Query instance
     */
    public function getNotesByComputerIdAndSearchV1($computerId, $search)
    {
        $query = $this->createQueryBuilder()
            ->sort('title', 'asc');

        if ($computerId) {
            $query->field('computer.id')->equals($computerId);
        }

        if ($search) {
            $query->addOr($query->expr()->field('content')->equals(new \MongoRegex('/.*'.$search.'.*/')));
            $query->addOr($query->expr()->field('title')->equals(new \MongoRegex('/.*'.$search.'.*/')));
        }

        $query->getQuery();

        return $query;
    }

    /**
     * Returns Note documents count by date end and date start.
     *
     * @param \DateTime|null $dateEnd   An end date
     * @param \DateTime|null $dateStart A start date
     *
     * @return integer
     */
    public function getNotesByDateEndAndDateStartV1($dateEnd, $dateStart)
    {
        $query = $this->createQueryBuilder();

        if ($dateEnd) {
            $query->field('createdAt')->lt($dateEnd);
        }

        if ($dateStart) {
            $query->field('createdAt')->gt($dateStart);
        }

        return $query->getQuery()
            ->execute()
            ->count();
    }

    /**
     * Removes Note documents by computer id.
     *
     * @param string $computerId A Computer document id
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function removeNotesByComputerIdV1($computerId)
    {
        return $this->createQueryBuilder()
            ->remove()
            ->field('computer.id')->equals($computerId)
            ->getQuery()
            ->execute();
    }
}
