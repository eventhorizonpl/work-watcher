<?php

namespace EventHorizon\WorkWatcherBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class ComputerRepository extends DocumentRepository
{
    /**
     * Returns Computer document by id and key.
     *
     * @param string $computerId A Computer document id
     * @param string $key        Key
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Computer|null A Computer instance
     */
    public function getComputerByIdAndKeyV1($computerId, $key)
    {
        return $this->createQueryBuilder()
            ->field('id')->equals($computerId)
            ->field('key')->equals($key)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Returns all Computer documents ordered by name ASC.
     *
     * @return \Doctrine\ODM\MongoDB\Query\Query A Query instance
     */
    public function getComputersV1()
    {
        return $this->createQueryBuilder()
            ->sort('name', 'asc')
            ->getQuery();
    }

    /**
     * Returns all Computer documents ordered by name ASC.
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function getComputersV3()
    {
        return $this->createQueryBuilder()
            ->sort('name', 'asc')
            ->getQuery()
            ->execute();
    }

    /**
     * Returns all Computer documents ordered by noteCounter DESC.
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function getComputersV4()
    {
        return $this->createQueryBuilder()
            ->sort('noteCounter', 'desc')
            ->getQuery()
            ->execute();
    }

    /**
     * Returns Computer documents count by date end and date start.
     *
     * @param \DateTime|null $dateEnd   An end date
     * @param \DateTime|null $dateStart A start date
     *
     * @return integer
     */
    public function getComputersByDateEndAndDateStartV1($dateEnd, $dateStart)
    {
        $query = $this->createQueryBuilder();

        if ($dateEnd) {
            $query->field('createdAt')->lt($dateEnd);
        }

        if ($dateStart) {
            $query->field('createdAt')->gt($dateStart);
        }

        return $query->getQuery()
            ->execute()
            ->count();
    }

    /**
     * Returns Computer documents by group ids ordered by name ASC.
     *
     * @param array $groupIds A Group documents ids array
     *
     * @return \Doctrine\ODM\MongoDB\Cursor A Cursor instance
     */
    public function getComputersByGroupIdsV1(array $groupIds)
    {
        $query = $this->createQueryBuilder()
            ->field('group.id')->in($groupIds)
            ->sort('name', 'asc')
            ->getQuery()
            ->execute();

        return $query;
    }

    /**
     * Returns Computer documents by group id and search.
     *
     * @param string $groupId A Group document id
     * @param string $search  A search string
     *
     * @return \Doctrine\ODM\MongoDB\Query\Query A Query instance
     */
    public function getComputersByGroupIdAndSearchV1($groupId, $search)
    {
        $query = $this->createQueryBuilder()
            ->sort('name', 'asc');

        if ($groupId) {
            $query->field('group.id')->equals($groupId);
        }

        if ($search) {
            $query->addOr($query->expr()->field('description')->equals(new \MongoRegex('/.*'.$search.'.*/')));
            $query->addOr($query->expr()->field('key')->equals(new \MongoRegex('/.*'.$search.'.*/')));
            $query->addOr($query->expr()->field('name')->equals(new \MongoRegex('/.*'.$search.'.*/')));
        }

        $query->getQuery();

        return $query;
    }

    /**
     * Returns Computer documents by group ids and search.
     *
     * @param array  $groupIds A Group documents ids array
     * @param string $search   A search string
     *
     * @return \Doctrine\ODM\MongoDB\Query\Query A Query instance
     */
    public function getComputersByGroupIdsAndSearchV1(array $groupIds, $search)
    {
        $query = $this->createQueryBuilder()
            ->field('group.id')->in($groupIds)
            ->sort('name', 'asc');

        if ($search) {
            $query->addOr($query->expr()->field('description')->equals(new \MongoRegex('/.*'.$search.'.*/')));
            $query->addOr($query->expr()->field('key')->equals(new \MongoRegex('/.*'.$search.'.*/')));
            $query->addOr($query->expr()->field('name')->equals(new \MongoRegex('/.*'.$search.'.*/')));
        }

        $query->getQuery();

        return $query;
    }
}
