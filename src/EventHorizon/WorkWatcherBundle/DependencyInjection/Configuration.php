<?php

namespace EventHorizon\WorkWatcherBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('event_horizon_work_watcher');

        $rootNode->children()
            ->integerNode('computer_fixtures')->min(0)->end()
            ->booleanNode('development_fixtures')->defaultFalse()->end()
            ->booleanNode('faker')->defaultFalse()->end()
            ->integerNode('group_fixtures')->min(0)->end()
            ->integerNode('note_fixtures')->min(0)->end()
            ->integerNode('report_fixtures')->min(0)->end()
            ->integerNode('user_fixtures')->min(0)->end()
        ->end();

        return $treeBuilder;
    }
}
