<?php

namespace EventHorizon\WorkWatcherBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 */
class EventHorizonWorkWatcherExtension extends Extension
{
    /**
     * Loads a specific configuration.
     *
     * @param array            $config    An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('event_horizon_work_watcher_computer_fixtures', $config['computer_fixtures']);
        $container->setParameter('event_horizon_work_watcher_development_fixtures', $config['development_fixtures']);
        $container->setParameter('event_horizon_work_watcher_faker', $config['faker']);
        $container->setParameter('event_horizon_work_watcher_group_fixtures', $config['group_fixtures']);
        $container->setParameter('event_horizon_work_watcher_note_fixtures', $config['note_fixtures']);
        $container->setParameter('event_horizon_work_watcher_report_fixtures', $config['report_fixtures']);
        $container->setParameter('event_horizon_work_watcher_user_fixtures', $config['user_fixtures']);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
    }
}
