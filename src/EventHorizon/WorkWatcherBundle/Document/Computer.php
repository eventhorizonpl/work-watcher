<?php

namespace EventHorizon\WorkWatcherBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ExclusionPolicy("all")
 * @MongoDB\Document(repositoryClass="EventHorizon\WorkWatcherBundle\Repository\ComputerRepository")
 */
class Computer
{
    use \EventHorizon\CoreBundle\Document\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Document\BlameableTrait;
    use \EventHorizon\CoreBundle\Document\TimestampableTrait;

    /**
     * @var \EventHorizon\SecurityBundle\Document\Group
     *
     * @Assert\Type(type="EventHorizon\SecurityBundle\Document\Group")
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\SecurityBundle\Document\Group")
     */
    private $group;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="EventHorizon\WorkWatcherBundle\Document\Note")
     */
    private $notes;

    /**
     * @var \EventHorizon\SecurityBundle\Document\User
     *
     * @Assert\Type(type="EventHorizon\SecurityBundle\Document\User")
     * @Expose
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\SecurityBundle\Document\User")
     */
    private $user;

    /**
     * @var integer
     *
     * @Assert\Range(
     *   min = 5,
     *   max = 3600,
     * )
     * @Assert\Type(type="integer")
     * @Expose
     * @MongoDB\Int
     */
    private $clientDelay = 600;

    /**
     * @var string
     *
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\String
     */
    private $description;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Regex("/^[a-zA-Z0-9\d]+$/")
     * @Assert\Type(type="string")
     * @MongoDB\Index
     * @MongoDB\String
     */
    private $key;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\Index
     * @MongoDB\String
     */
    private $name;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     * @Expose
     * @MongoDB\Index
     * @MongoDB\Int
     */
    private $noteCounter = 0;

    /**
     * @var boolean
     *
     * @Expose
     * @MongoDB\Boolean
     */
    private $overwriteClientConfiguration = false;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }

    /**
     * __toString.
     *
     * @return string
     */
    public function __toString()
    {
        return ($this->getName()) ?: '';
    }

    /**
     * Get clientDelay.
     *
     * @return integer $clientDelay
     */
    public function getClientDelay()
    {
        return $this->clientDelay;
    }

    /**
     * Set clientDelay.
     *
     * @param integer $clientDelay
     *
     * @return Computer
     */
    public function setClientDelay($clientDelay)
    {
        $this->clientDelay = $clientDelay;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Computer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Set object data from array.
     *
     * @param array $data
     *
     * @return Computer
     */
    public function setFromArray(array $data)
    {
        if (array_key_exists('clientDelay', $data)) {
            $this->setClientDelay($data['clientDelay']);
        }

        if (array_key_exists('description', $data)) {
            $this->setDescription($data['description']);
        }

        if (array_key_exists('group', $data)) {
            $this->setGroup($data['group']);
        }

        if (array_key_exists('key', $data)) {
            $this->setKey($data['key']);
        }

        if (array_key_exists('name', $data)) {
            $this->setName($data['name']);
        }

        if (array_key_exists('overwriteClientConfiguration', $data)) {
            $this->setOverwriteClientConfiguration($data['overwriteClientConfiguration']);
        }

        if (array_key_exists('user', $data)) {
            $this->setUser($data['user']);
        }

        return $this;
    }

    /**
     * Get group.
     *
     * @return \EventHorizon\SecurityBundle\Document\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set group.
     *
     * @param \EventHorizon\SecurityBundle\Document\Group $group
     *
     * @return Computer
     */
    public function setGroup(\EventHorizon\SecurityBundle\Document\Group $group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get key.
     *
     * @return string $key
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set key.
     *
     * @param string $key
     *
     * @return Computer
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Computer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add note.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Note $note
     *
     * @return Computer
     */
    public function addNote(Note $note)
    {
        $this->notes[] = $note;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Remove note.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Note $note
     */
    public function removeNote(Note $note)
    {
        $this->notes->removeElement($note);

        return $this;
    }

    /**
     * Decrement noteCounter.
     *
     * @return Computer
     */
    public function decNoteCounter()
    {
        $this->noteCounter = $this->noteCounter - 1;

        return $this;
    }

    /**
     * Get noteCounter.
     *
     * @return integer $noteCounter
     */
    public function getNoteCounter()
    {
        return $this->noteCounter;
    }

    /**
     * Increment noteCounter.
     *
     * @return Computer
     */
    public function incNoteCounter()
    {
        $this->noteCounter = $this->noteCounter + 1;

        return $this;
    }

    /**
     * Get overwriteClientConfiguration.
     *
     * @return boolean $overwriteClientConfiguration
     */
    public function getOverwriteClientConfiguration()
    {
        return $this->overwriteClientConfiguration;
    }

    /**
     * Set overwriteClientConfiguration.
     *
     * @param boolean $overwriteClientConfiguration
     *
     * @return Computer
     */
    public function setOverwriteClientConfiguration($overwriteClientConfiguration)
    {
        $this->overwriteClientConfiguration = $overwriteClientConfiguration;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \EventHorizon\SecurityBundle\Document\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $user
     *
     * @return Computer
     */
    public function setUser(\EventHorizon\SecurityBundle\Document\User $user)
    {
        $this->user = $user;

        return $this;
    }
}
