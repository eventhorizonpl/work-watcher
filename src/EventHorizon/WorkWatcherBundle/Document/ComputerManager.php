<?php

namespace EventHorizon\WorkWatcherBundle\Document;

class ComputerManager
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContext
     */
    private $security;

    /**
     * Constructor.
     *
     * @param \Doctrine\ODM\MongoDB\DocumentManager            $dm
     * @param \Symfony\Component\Security\Core\SecurityContext $security
     */
    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $dm, \Symfony\Component\Security\Core\SecurityContext $security)
    {
        $this->dm = $dm;
        $this->security = $security;
    }

    /**
     * Creates a Computer document.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $user An User instance
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Computer
     */
    public function create(\EventHorizon\SecurityBundle\Document\User $user)
    {
        $computer = new Computer();
        $computer->setUser($user);
        $computer->setKey(substr(md5($user.date("Y-m-d H:i:s")), 0, 8));

        return $computer;
    }

    /**
     * Delete a Computer document.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $computer A Computer instance
     */
    public function delete(\EventHorizon\WorkWatcherBundle\Document\Computer $computer)
    {
        $this->dm->remove($computer);
        $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->removeReportsByComputerIdV1($computer->getId());
        $this->dm->getRepository('EventHorizonWorkWatcherBundle:Note')->removeNotesByComputerIdV1($computer->getId());
        $this->dm->flush();
    }

    /**
     * Retrive Computer documents and Group documents.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $user    An User instance
     * @param string                                     $groupId A Group id
     * @param string                                     $query   A search string
     *
     * @return array
     */
    public function retriveComputersAndGroups(\EventHorizon\SecurityBundle\Document\User $user, $groupId, $query)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $groups = $this->dm->getRepository('EventHorizonSecurityBundle:Group')->findAll();

            $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersByGroupIdAndSearchV1($groupId, $query);
        } elseif ($this->security->isGranted('ROLE_OBSERVER')) {
            $groups = $user->getGroups();

            if ($groupId) {
                $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersByGroupIdAndSearchV1($groupId, $query);
            } else {
                $groupIds = array();
                foreach ($groups as $group) {
                    $groupIds[] = $group->getId();
                }

                $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersByGroupIdsAndSearchV1($groupIds, $query);
            }
        }

        return array(
            'computers' => $computers,
            'groups' => $groups,
        );
    }

    /**
     * Saves a Computer document.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $computer A Computer instance
     */
    public function save(\EventHorizon\WorkWatcherBundle\Document\Computer $computer)
    {
        $this->dm->persist($computer);
        $this->dm->flush();
    }
}
