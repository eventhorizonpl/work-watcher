<?php

namespace EventHorizon\WorkWatcherBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ExclusionPolicy("all")
 * @MongoDB\Document(repositoryClass="EventHorizon\WorkWatcherBundle\Repository\ReportRepository")
 */
class Report
{
    use \EventHorizon\CoreBundle\Document\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Document\FileTrait;
    use \EventHorizon\CoreBundle\Document\TimestampableTrait;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\Computer
     *
     * @Assert\Type(type="EventHorizon\WorkWatcherBundle\Document\Computer")
     * @Expose
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\WorkWatcherBundle\Document\Computer")
     */
    private $computer;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\Note
     *
     * @Assert\Type(type="EventHorizon\WorkWatcherBundle\Document\Note")
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\WorkWatcherBundle\Document\Note")
     */
    private $note;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\Index
     * @MongoDB\String
     */
    private $date;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\String
     */
    private $hostname;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\Index
     * @MongoDB\String
     */
    private $ip;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\String
     */
    private $time;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @Expose
     * @MongoDB\String
     */
    private $username;

    /**
     * __toString.
     *
     * @return string
     */
    public function __toString()
    {
        return ($this->getIp().' '.$this->getDate()) ?: '';
    }

    /**
     * Get computer.
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Computer
     */
    public function getComputer()
    {
        return $this->computer;
    }

    /**
     * Set computer.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Computer $computer
     *
     * @return Report
     */
    public function setComputer(\EventHorizon\WorkWatcherBundle\Document\Computer $computer)
    {
        $this->computer = $computer;

        return $this;
    }

    /**
     * Get date.
     *
     * @return string $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date.
     *
     * @param string $date
     *
     * @return Report
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set object data from array.
     *
     * @param array $data
     *
     * @return Computer
     */
    public function setFromArray(array $data)
    {
        if (array_key_exists('computer', $data)) {
            $this->setComputer($data['computer']);
        }

        if (array_key_exists('note', $data)) {
            $this->setNote($data['note']);
        }

        if (array_key_exists('date', $data)) {
            $this->setDate($data['date']);
        }

        if (array_key_exists('hostname', $data)) {
            $this->setHostname($data['hostname']);
        }

        if (array_key_exists('ip', $data)) {
            $this->setIp($data['ip']);
        }

        if (array_key_exists('time', $data)) {
            $this->setTime($data['time']);
        }

        if (array_key_exists('username', $data)) {
            $this->setUsername($data['username']);
        }

        return $this;
    }

    /**
     * Get hostname.
     *
     * @return string $hostname
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * Set hostname.
     *
     * @param string $hostname
     *
     * @return Report
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return string $ip
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return Report
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Clear note.
     *
     * @return Report
     */
    public function clearNote()
    {
        $this->note = null;

        return $this;
    }

    /**
     * Get note.
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Note
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Note $note
     *
     * @return Report
     */
    public function setNote(\EventHorizon\WorkWatcherBundle\Document\Note $note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get time.
     *
     * @return string $time
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set time.
     *
     * @param string $time
     *
     * @return Report
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string $username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     *
     * @return Report
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }
}
