<?php

namespace EventHorizon\WorkWatcherBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(repositoryClass="EventHorizon\WorkWatcherBundle\Repository\NoteRepository")
 * @MongoDB\HasLifecycleCallbacks
 */
class Note
{
    use \EventHorizon\CoreBundle\Document\IdentifiableTrait;
    use \EventHorizon\CoreBundle\Document\BlameableTrait;
    use \EventHorizon\CoreBundle\Document\TimestampableTrait;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\Computer
     *
     * @Assert\Type(type="EventHorizon\WorkWatcherBundle\Document\Computer")
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\WorkWatcherBundle\Document\Computer")
     */
    private $computer;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\Report
     *
     * @Assert\Type(type="EventHorizon\WorkWatcherBundle\Document\Report")
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\WorkWatcherBundle\Document\Report")
     */
    private $report;

    /**
     * @var \EventHorizon\SecurityBundle\Document\User
     *
     * @Assert\Type(type="EventHorizon\SecurityBundle\Document\User")
     * @MongoDB\Index
     * @MongoDB\ReferenceOne(targetDocument="EventHorizon\SecurityBundle\Document\User")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @MongoDB\String
     */
    private $content;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     * @MongoDB\String
     */
    private $title;

    /**
     * Get computer.
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Computer
     */
    public function getComputer()
    {
        return $this->computer;
    }

    /**
     * Set computer.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Computer $computer
     *
     * @return Note
     */
    public function setComputer(\EventHorizon\WorkWatcherBundle\Document\Computer $computer)
    {
        $computer->addNote($this);
        $computer->incNoteCounter();
        $this->computer = $computer;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return Note
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Set object data from array.
     *
     * @param array $data
     *
     * @return Note
     */
    public function setFromArray(array $data)
    {
        if (array_key_exists('computer', $data)) {
            $this->setComputer($data['computer']);
        }

        if (array_key_exists('content', $data)) {
            $this->setContent($data['content']);
        }

        if (array_key_exists('report', $data)) {
            $this->setReport($data['report']);
        }

        if (array_key_exists('title', $data)) {
            $this->setTitle($data['title']);
        }

        if (array_key_exists('user', $data)) {
            $this->setUser($data['user']);
        }

        return $this;
    }

    /**
     * Get report.
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set report.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Report $report
     *
     * @return Note
     */
    public function setReport(\EventHorizon\WorkWatcherBundle\Document\Report $report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Note
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \EventHorizon\SecurityBundle\Document\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $user
     *
     * @return Note
     */
    public function setUser(\EventHorizon\SecurityBundle\Document\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @MongoDB\PreRemove()
     */
    public function updateComputerStatus()
    {
        $this->getComputer()->decNoteCounter();
        $this->getComputer()->removeNote($this);
    }
}
