<?php

namespace EventHorizon\WorkWatcherBundle\Document;

class NoteManager
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContext
     */
    private $security;

    /**
     * Constructor.
     *
     * @param \Doctrine\ODM\MongoDB\DocumentManager            $dm
     * @param \Symfony\Component\Security\Core\SecurityContext $security
     */
    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $dm, \Symfony\Component\Security\Core\SecurityContext $security)
    {
        $this->dm = $dm;
        $this->security = $security;
    }

    /**
     * Creates a Note document.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Report $report A Report instance
     * @param \EventHorizon\SecurityBundle\Document\User      $user   An User instance
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Note
     */
    public function create(\EventHorizon\WorkWatcherBundle\Document\Report $report, \EventHorizon\SecurityBundle\Document\User $user)
    {
        $note = new Note();
        $note->setComputer($report->getComputer());
        $note->setReport($report);
        $note->setUser($user);

        return $note;
    }

    /**
     * Delete a Note document.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Note $note A Note instance
     */
    public function delete(\EventHorizon\WorkWatcherBundle\Document\Note $note)
    {
        $this->dm->remove($note);
        $this->dm->flush();
    }

    /**
     * Retrive Computer documents and Note documents.
     *
     * @param \EventHorizon\SecurityBundle\Document\User $user       An User instance
     * @param string                                     $computerId A Computer id
     * @param string                                     $query      A search string
     *
     * @return array
     */
    public function retriveComputersAndNotes(\EventHorizon\SecurityBundle\Document\User $user, $computerId, $query)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersV3();

            $notes = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Note')->getNotesByComputerIdAndSearchV1($computerId, $query);
        } elseif ($this->security->isGranted('ROLE_OBSERVER')) {
            $groupIds = array();
            foreach ($user->getGroups() as $group) {
                $groupIds[] = $group->getId();
            }

            $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersByGroupIdsV1($groupIds);

            $notes = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Note')->getNotesByComputerIdAndUserAndSearchV1($computerId, $user, $query);
        }

        return array(
            'computers' => $computers,
            'notes' => $notes,
        );
    }

    /**
     * Retrive Note documents.
     *
     * @param string $computerId A Computer id
     * @param string $dateEnd    An end date
     * @param string $dateStart  A start date
     *
     * @return array
     */
    public function retriveNotes($computerId, $dateEnd, $dateStart)
    {
        if (null === $dateEnd) {
            $dateEnd = new \DateTime(date("Y-m-d"));
        } else {
            $dateEnd = new \DateTime($dateEnd);
        }
        $dateEnd->setTime(23, 59, 59);

        if (null === $dateStart) {
            $dateStart = new \DateTime(date("Y-m-d"));
            $dateStart->modify('-1 month');
        } else {
            $dateStart = new \DateTime($dateStart);
        }
        $dateStart->setTime(0, 0, 0);

        $notes = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Note')->getNotesByComputerIdAndDateEndAndDateStartV1($computerId, $dateEnd, $dateStart);

        return array(
            'dateEnd' => $dateEnd,
            'dateStart' => $dateStart,
            'notes' => $notes,
        );
    }

    /**
     * Saves a Note document.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Note $note A Note instance
     */
    public function save(\EventHorizon\WorkWatcherBundle\Document\Note $note)
    {
        $this->dm->persist($note);
        $this->dm->flush();
    }
}
