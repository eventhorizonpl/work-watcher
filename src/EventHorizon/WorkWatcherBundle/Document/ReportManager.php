<?php

namespace EventHorizon\WorkWatcherBundle\Document;

class ReportManager
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $dm;

    /**
     * @var \Symfony\Component\Validator\Validator\LegacyValidator
     */
    private $validator;

    /**
     * Constructor.
     *
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    public function __construct(\Doctrine\ODM\MongoDB\DocumentManager $dm, \Symfony\Component\Validator\Validator\LegacyValidator $validator)
    {
        $this->dm = $dm;
        $this->validator = $validator;
    }

    /**
     * Create Report document.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Computer   $computer A Computer instance
     * @param string                                              $date     A report date
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file     A file
     * @param string                                              $hostname Client hostname
     * @param string                                              $ip       Client IP address
     * @param string                                              $time     A report time
     * @param string                                              $username Client username
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Report
     */
    public function createReport(\EventHorizon\WorkWatcherBundle\Document\Computer $computer, $date, \Symfony\Component\HttpFoundation\File\UploadedFile $file, $hostname, $ip, $time, $username)
    {
        $report = new Report();
        $report->setComputer($computer);
        $report->setDate($date);
        $report->setHostname($hostname);
        $report->setIp($ip);
        $report->setTime($time);
        $report->setUsername($username);
        $tmpName = null;
        if ($file) {
            if ($file->getClientMimeType() == "image/png") {
                $tmpName = $file->getPathname().md5(microtime()).".jpg";
                $image = imagecreatefrompng($file->getPathname());
                imagejpeg($image, $tmpName, 75);
                $report->setFile($tmpName);
                imagedestroy($image);
                $report->setFilename(str_replace("png", "jpg", $file->getClientOriginalName()));
                $report->setMimeType("image/jpeg");
            } else {
                $report->setFile($file->getPathname());
                $report->setFilename($file->getClientOriginalName());
                $report->setMimeType($file->getClientMimeType());
            }
        }

        $errors = $this->validator->validate($report);

        if (0 === count($errors)) {
            $this->save($report);
        }

        if ($tmpName) {
            unlink($tmpName);
        }

        return $report;
    }

    /**
     * Retrive Report documents.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Computer $computer  A Computer instance
     * @param string                                            $dateEnd   An end date
     * @param string                                            $dateStart A start date
     *
     * @return array
     */
    public function retriveReports(\EventHorizon\WorkWatcherBundle\Document\Computer $computer, $dateEnd, $dateStart)
    {
        if (null === $dateEnd) {
            $dateEnd = new \DateTime(date("Y-m-d"));
        } else {
            $dateEnd = new \DateTime($dateEnd);
        }
        $dateEnd->setTime(23, 59, 59);

        if (null === $dateStart) {
            $dateStart = new \DateTime(date("Y-m-d"));
        } else {
            $dateStart = new \DateTime($dateStart);
        }
        $dateStart->setTime(0, 0, 0);

        $reports = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->getReportsByComputerIdAndDateEndAndDateStartV1($computer, $dateEnd, $dateStart);

        return array(
            'dateEnd' => $dateEnd,
            'dateStart' => $dateStart,
            'reports' => $reports,
        );
    }

    /**
     * Saves a Report document.
     *
     * @param \EventHorizon\WorkWatcherBundle\Document\Report $report A Report instance
     */
    public function save(\EventHorizon\WorkWatcherBundle\Document\Report $report)
    {
        $this->dm->persist($report);
        $this->dm->flush();
    }
}
