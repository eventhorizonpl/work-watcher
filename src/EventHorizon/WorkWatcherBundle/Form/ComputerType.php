<?php

namespace EventHorizon\WorkWatcherBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ComputerType extends AbstractType
{
    /**
     * Builds the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clientDelay')
            ->add('description', 'textarea')
            ->add('group')
            ->add('key')
            ->add('name')
            ->add('overwriteClientConfiguration', 'choice', array(
                'choices'  => array(
                    '0' => 'no',
                    '1' => 'yes',
                ),
            ))
        ;
    }

    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolverInterface $resolver The resolver for the options.
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\WorkWatcherBundle\Document\Computer',
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return 'eventhorizon_workwatcherbundle_computer_type';
    }
}
