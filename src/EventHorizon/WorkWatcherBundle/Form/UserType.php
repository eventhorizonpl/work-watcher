<?php

namespace EventHorizon\WorkWatcherBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    /**
     * Builds the form.
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('enabled', 'choice', array(
                'choices'  => array(
                    '0' => 'no',
                    '1' => 'yes',
                ),
            ))
            ->add('groups', null, array(
                'expanded' => false,
                'multiple' => true,
            ))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->add('roles', 'choice', array(
                'choices'  => array(
                    'ROLE_ADMIN' => 'ROLE_ADMIN',
                    'ROLE_OBSERVER' => 'ROLE_OBSERVER',
                ),
                'multiple' => true,
                'required' => false,
            ))
            ->add('username')
        ;
    }

    /**
     * Sets the default options for this type.
     *
     * @param OptionsResolverInterface $resolver The resolver for the options.
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EventHorizon\SecurityBundle\Document\User',
        ));
    }

    /**
     * Returns the name of this type.
     *
     * @return string
     */
    public function getName()
    {
        return 'eventhorizon_securitybundle_user_type';
    }
}
