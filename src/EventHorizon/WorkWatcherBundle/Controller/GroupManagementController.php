<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\WorkWatcherBundle\Form\GroupType;

/**
 * GroupManagement controller.
 *
 * @Route("/group_management")
 */
class GroupManagementController extends Controller
{
    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * @var \EventHorizon\SecurityBundle\Document\GroupManager
     *
     * @DI\Inject("event_horizon_security_group_document_manager")
     */
    private $groupManager;

    /**
     * @var \EventHorizon\LogBundle\Service\Log
     *
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @var \Knp\Component\Pager\Paginator
     *
     * @DI\Inject("knp_paginator")
     */
    private $paginator;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * Creates a form for a Group document.
     *
     * @param Group  $group  A Group instance
     * @param string $method Form submit method
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createGroupForm(Group $group, $method)
    {
        $form = $this->createForm(new GroupType(), $group, array(
            'method' => $method,
        ));

        return $form;
    }

    /**
     * Returns a Group document.
     *
     * @param string $groupId A Group document id
     *
     * @return \EventHorizon\SecurityBundle\Document\Group
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getGroupObject($groupId)
    {
        $group = $this->dm->getRepository('EventHorizonSecurityBundle:Group')->find($groupId);

        if (null === $group) {
            $this->message = "EventHorizonWorkWatcherBundle:GroupManagement:getGroupObject id=".$groupId." userId=".$this->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find Group document.');
        }

        return $group;
    }

    /**
     * Lists all Group documents.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/", name="group_management_index")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:GroupManagement:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $limit = $request->query->get('limit', 20);
        $page = $request->query->get('page', 1);
        $query = $request->query->get('query');
        $groups = $this->dm->getRepository('EventHorizonSecurityBundle:Group')->getGroupsBySearchV1($query);
        $pagination = $this->paginator->paginate($groups, $page, $limit);

        return array(
            'pagination' => $pagination,
            'query' => $query,
        );
    }

    /**
     * Displays a form to create a new Group document.
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/new", name="group_management_new")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:GroupManagement:new.html.twig")
     */
    public function newAction()
    {
        $group = $this->groupManager->create();
        $form = $this->createGroupForm($group, 'POST');

        return array(
            'form' => $form->createView(),
            'group' => $group,
        );
    }

    /**
     * Creates a new Group document.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("POST")
     * @Route("/new", name="group_management_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:GroupManagement:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $group = $this->groupManager->create();
        $form = $this->createGroupForm($group, 'POST');
        $form->handleRequest($request);

        if (true === $form->isValid()) {
            $this->groupManager->save($group);
            $this->addFlash('success', $this->translator->trans('group_created'));

            return $this->redirectToRoute('group_management_index');
        }

        return array(
            'form' => $form->createView(),
            'group' => $group,
        );
    }

    /**
     * Displays a form to edit an existing Group document.
     *
     * @param string $id A Group document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{id}/edit", name="group_management_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:GroupManagement:edit.html.twig")
     */
    public function editAction($id)
    {
        $group = $this->getGroupObject($id);
        $editForm = $this->createGroupForm($group, 'PUT');

        return array(
            'form' => $editForm->createView(),
            'group' => $group,
        );
    }

    /**
     * Edits an existing Group document.
     *
     * @param Request $request A Request instance
     * @param string  $id      A Group document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("PUT")
     * @Route("/{id}/edit", name="group_management_update")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:GroupManagement:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $group = $this->getGroupObject($id);
        $editForm = $this->createGroupForm($group, 'PUT');
        $editForm->handleRequest($request);

        if (true === $editForm->isValid()) {
            $this->groupManager->save($group);
            $this->addFlash('success', $this->translator->trans('group_updated'));

            return $this->redirectToRoute('group_management_index');
        }

        return array(
            'form' => $editForm->createView(),
            'group' => $group,
        );
    }
}
