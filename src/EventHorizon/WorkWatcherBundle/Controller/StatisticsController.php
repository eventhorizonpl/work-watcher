<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Statistics controller.
 *
 * @Route("/statistics")
 */
class StatisticsController extends Controller
{
    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * Statistics.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/", name="statistics_index")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Statistics:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $dateEnd = $request->query->get('date_end');
        $dateStart = $request->query->get('date_start');

        if ($dateEnd) {
            $dateEnd = new \DateTime($dateEnd);
            $dateEnd->setTime(23, 59, 59);
        }

        if ($dateStart) {
            $dateStart = new \DateTime($dateStart);
            $dateStart->setTime(0, 0, 0);
        }

        $computersCount = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersByDateEndAndDateStartV1($dateEnd, $dateStart);
        $notesCount = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Note')->getNotesByDateEndAndDateStartV1($dateEnd, $dateStart);
        $reportsCount = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->getReportsByDateEndAndDateStartV1($dateEnd, $dateStart);

        return array(
            'computersCount' => $computersCount,
            'dateEnd' => $dateEnd,
            'dateStart' => $dateStart,
            'notesCount' => $notesCount,
            'reportsCount' => $reportsCount,
        );
    }

    /**
     * Statistics - notes per computer.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/notes-per-computer", name="statistics_notes_per_computer")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Statistics:notesPerComputer.html.twig")
     */
    public function notesPerComputerAction(Request $request)
    {
        $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersV4();
        $highestNotesCount = 0;

        foreach ($computers as $computer) {
            if ($computer->getNoteCounter() > $highestNotesCount) {
                $highestNotesCount = $computer->getNoteCounter();
            }
        }

        return array(
            'computers' => $computers,
            'highestNotesCount' => $highestNotesCount,
        );
    }
}
