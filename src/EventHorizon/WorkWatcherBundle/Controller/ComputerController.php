<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Form\ComputerType;

/**
 * Computer controller.
 *
 * @Route("/computer")
 */
class ComputerController extends Controller
{
    use \EventHorizon\WorkWatcherBundle\Controller\Traits\GetComputerObjectTrait;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\ComputerManager
     *
     * @DI\Inject("event_horizon_work_watcher_computer_manager")
     */
    private $computerManager;

    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * @var \EventHorizon\LogBundle\Service\Log
     *
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @var \Knp\Component\Pager\Paginator
     *
     * @DI\Inject("knp_paginator")
     */
    private $paginator;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * Creates a Computer document.
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Computer
     */
    private function createComputerObject()
    {
        $computer = $this->computerManager->create($this->getUser());

        return $computer;
    }

    /**
     * Creates a form for a Computer document.
     *
     * @param Computer $computer A Computer instance
     * @param string   $method   Form submit method
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createComputerForm(Computer $computer, $method)
    {
        $form = $this->createForm(new ComputerType(), $computer, array(
            'method' => $method,
        ));

        return $form;
    }

    /**
     * Lists all Computer documents.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Cache(maxage="0", smaxage="0")
     *
     * @Method("GET")
     * @Route("/", name="computer_index")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Computer:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $groupId = $request->query->get('group_id');
        $limit = $request->query->get('limit', 20);
        $page = $request->query->get('page', 1);
        $query = $request->query->get('query');
        $results = $this->computerManager->retriveComputersAndGroups($this->getUser(), $groupId, $query);
        $pagination = $this->paginator->paginate($results['computers'], $page, $limit);

        return array(
            'groupId' => $groupId,
            'groups' => $results['groups'],
            'pagination' => $pagination,
            'query' => $query,
        );
    }

    /**
     * Lists all Computer documents.
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/print", name="computer_print")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:Computer:print.html.twig")
     */
    public function printAction()
    {
        $computers = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputersV1();

        return array(
            'computers' => $computers,
        );
    }

    /**
     * Displays a form to create a new Computer document.
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/new", name="computer_new")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:Computer:new.html.twig")
     */
    public function newAction()
    {
        $computer = $this->createComputerObject();
        $form = $this->createComputerForm($computer, 'POST');

        return array(
            'computer' => $computer,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new Computer document.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("POST")
     * @Route("/new", name="computer_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:Computer:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $computer = $this->createComputerObject();
        $form = $this->createComputerForm($computer, 'POST');
        $form->handleRequest($request);

        if (true === $form->isValid()) {
            $this->computerManager->save($computer);
            $this->addFlash('success', $this->translator->trans('computer_created'));

            return $this->redirectToRoute('computer_index');
        }

        return array(
            'computer' => $computer,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Computer document.
     *
     * @param string $id A Computer document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{id}/edit", name="computer_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:Computer:edit.html.twig")
     */
    public function editAction($id)
    {
        $computer = $this->getComputerObject($id);
        $editForm = $this->createComputerForm($computer, 'PUT');

        return array(
            'computer' => $computer,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Edits an existing Computer document.
     *
     * @param Request $request A Request instance
     * @param string  $id      A Computer document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("PUT")
     * @Route("/{id}/edit", name="computer_update")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:Computer:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $computer = $this->getComputerObject($id);
        $editForm = $this->createComputerForm($computer, 'PUT');
        $editForm->handleRequest($request);

        if (true === $editForm->isValid()) {
            $this->computerManager->save($computer);
            $this->addFlash('success', $this->translator->trans('computer_updated'));

            return $this->redirectToRoute('computer_index');
        }

        return array(
            'computer' => $computer,
            'form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a Computer document.
     *
     * @param string $id A Computer document id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse A RedirectResponse instance
     *
     * @Method("GET")
     * @Route("/{id}/delete", name="computer_delete")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteAction($id)
    {
        $computer = $this->getComputerObject($id);
        $this->computerManager->delete($computer);
        $this->addFlash('success', $this->translator->trans('computer_deleted'));

        return $this->redirectToRoute('computer_index');
    }
}
