<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EventHorizon\CoreBundle\HttpFoundation\StreamedResponse;

/**
 * Report controller.
 *
 * @Route("/report")
 */
class ReportController extends Controller
{
    use \EventHorizon\WorkWatcherBundle\Controller\Traits\GetComputerObjectTrait;
    use \EventHorizon\WorkWatcherBundle\Controller\Traits\GetReportObjectTrait;

    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * @var \EventHorizon\LogBundle\Service\Log
     *
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\ReportManager
     *
     * @DI\Inject("event_horizon_work_watcher_report_manager")
     */
    private $reportManager;

    /**
     * Shows report file.
     *
     * @param Request $request A Request instance
     * @param string  $id      A Report document id
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse A StreamedResponse instance
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @Method("GET")
     * @Route("/file-show/{id}", name="report_file_show")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     */
    public function fileShowAction(Request $request, $id)
    {
        $report = $this->getReportObject($id);

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', $report->getMimeType());

        if ($report->getFile()->getMongoGridFSFile()->getSize()) {
            $stream = $report->getFile()->getMongoGridFSFile()->getResource();

            $response->setCallback(function () use ($stream) {
                fpassthru($stream);
            });
        } else {
            throw $this->createNotFoundException("report photo was not found");
        }

        return $response;
    }

    /**
     * Shows list of reports.
     *
     * @param Request $request    A Request instance
     * @param string  $computerId A Computer document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{computerId}/show-reports", name="report_show")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Report:showReports.html.twig")
     */
    public function showReportAction(Request $request, $computerId)
    {
        $computer = $this->getComputerObject($computerId);
        $dateEnd = $request->query->get('date_end');
        $dateStart = $request->query->get('date_start');

        $results = $this->reportManager->retriveReports($computer, $dateEnd, $dateStart);

        return array(
            'computer' => $computer,
            'dateEnd' => $results['dateEnd'],
            'dateStart' => $results['dateStart'],
            'reports' => $results['reports'],
        );
    }

    /**
     * Shows computer live view.
     *
     * @param string $computerId A Computer document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{computerId}/show-live-view", name="report_show_live_view")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Report:showLiveView.html.twig")
     */
    public function showLiveViewAction($computerId)
    {
        $computer = $this->getComputerObject($computerId);

        return array(
            'computer' => $computer,
        );
    }

    /**
     * Shows report file.
     *
     * @param string $id A Report document id
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse A StreamedResponse instance
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *
     * @Method("GET")
     * @Route("/{computerId}/show-live-view-file", name="report_show_live_view_file")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     */
    public function showLiveViewFileAction($computerId)
    {
        $report = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->getReportByComputerIdV1($computerId);

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', $report->getMimeType());

        if ($report->getFile()->getMongoGridFSFile()->getSize()) {
            $stream = $report->getFile()->getMongoGridFSFile()->getResource();

            $response->setCallback(function () use ($stream) {
                fpassthru($stream);
            });
        } else {
            throw $this->createNotFoundException("report photo was not found");
        }

        return $response;
    }
}
