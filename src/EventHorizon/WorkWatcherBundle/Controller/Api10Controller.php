<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use EventHorizon\WorkWatcherBundle\Document\Report;

/**
 * Api10 controller.
 *
 * @Route("/api")
 */
class Api10Controller extends FOSRestController
{
    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * @var \EventHorizon\LogBundle\Service\Log
     *
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\ReportManager
     *
     * @DI\Inject("event_horizon_work_watcher_report_manager")
     */
    private $reportManager;

    /**
     * Submit report for API v1.0.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     *
     * @ApiDoc(
     *   description="Submit report",
     *   parameters={
     *     {"name"="computer_id", "dataType"="string", "required"=true, "description"="computer id"},
     *     {"name"="computer_key", "dataType"="string", "required"=true, "description"="computer key"},
     *     {"name"="date", "dataType"="type", "required"=true, "description"="date Y-m-d"},
     *     {"name"="hostname", "dataType"="string", "required"=true, "description"="client hostname"},
     *     {"name"="time", "dataType"="type", "required"=true, "description"="time H:i:s"},
     *     {"name"="username", "dataType"="string", "required"=true, "description"="client username"},
     *     {"name"="file", "dataType"="file", "required"=true, "description"="screenshot"}
     *   },
     *   resource=true,
     *   section="API v1.0",
     *   statusCodes={
     *      200="Returned when successful",
     *      400="Returned when one of required parameters is not found",
     *      404="Returned when the computer is not found",
     *      500="Returned when something goes wrong on the server"
     *   },
     *   tags="stable"
     * )
     *
     * @Method("POST")
     * @Route("/v1.0/report/submit", name="api_v1_0_report_submit")
     */
    public function reportSubmit10Action(Request $request)
    {
        $ip = $request->getClientIp();
        $file = $request->files->get('file');
        $parameters = array(
            'computerId' => 'computer_id',
            'computerKey' => 'computer_key',
            'date' => 'date',
            'hostname' => 'hostname',
            'time' => 'time',
            'username' => 'username',
        );

        foreach ($parameters as $key => $value) {
            $$key = $request->request->get($value);
            if (null === $$key) {
                throw new BadRequestHttpException($value." was not specified");
            }
        }

        $view = $this->view();
        $view->setFormat('json');

        $computer = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->getComputerByIdAndKeyV1($computerId, $computerKey);

        if (null === $computer) {
            $this->message = "EventHorizonWorkWatcherBundle:Api:reportSubmit10Action computerId=".$computerId." computerKey=".$computerKey." date=".$date." IP=".$ip;
            $this->log->logNotice($this->message);

            throw new NotFoundHttpException("computer was not found");
        }

        try {
            $this->reportManager->createReport($computer, $date, $file, $hostname, $ip, $time, $username);

            $view->setData(array('status' => 'ok'));
            $view->setStatusCode(200);
        } catch (\Exception $e) {
            throw new HttpException(500, "internal server error");
        }

        return $this->handleView($view);
    }

    /**
     * Test function for API v1.0.
     *
     * @ApiDoc(
     *   description="Test API version",
     *   resource=true,
     *   section="API v1.0",
     *   statusCodes={
     *      200="Returned when successful"
     *   },
     *   tags="stable"
     * )
     *
     * @Method("GET")
     * @Route("/v1.0/test", name="api_v1_0_test")
     */
    public function test10Action()
    {
        $view = $this->view();
        $view->setFormat('json');
        $view->setData(array('status' => 'ok'));
        $view->setStatusCode(200);

        return $this->handleView($view);
    }
}
