<?php

namespace EventHorizon\WorkWatcherBundle\Controller\Traits;

trait GetReportObjectTrait
{
    /**
     * Returns a Report document.
     *
     * @param string $reportId A Report document id
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Report
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getReportObject($reportId)
    {
        $report = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->find($reportId);

        if (null === $report) {
            $this->message = "EventHorizonWorkWatcherBundle:".get_class($this).":getReportObject id=".$reportId." userId=".$this->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find Report document.');
        }

        return $report;
    }
}
