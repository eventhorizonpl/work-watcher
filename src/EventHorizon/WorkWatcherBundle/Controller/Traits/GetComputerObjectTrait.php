<?php

namespace EventHorizon\WorkWatcherBundle\Controller\Traits;

trait GetComputerObjectTrait
{
    /**
     * Returns a Computer document.
     *
     * @param string $computerId A Computer document id
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Computer
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getComputerObject($computerId)
    {
        $computer = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Computer')->find($computerId);

        if (null === $computer) {
            $this->message = "EventHorizonWorkWatcherBundle:".get_class($this).":getComputerObject id=".$computerId." userId=".$this->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find Computer document.');
        }

        return $computer;
    }
}
