<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Form\UserType;

/**
 * UserManagement controller.
 *
 * @Route("/user_management")
 */
class UserManagementController extends Controller
{
    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * @var \EventHorizon\LogBundle\Service\Log
     *
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @var \Knp\Component\Pager\Paginator
     *
     * @DI\Inject("knp_paginator")
     */
    private $paginator;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * @var \EventHorizon\SecurityBundle\Document\UserManager
     *
     * @DI\Inject("event_horizon_security_user_document_manager")
     */
    private $userManager;

    /**
     * Creates a form for a User document.
     *
     * @param User   $user   An User instance
     * @param string $method Form submit method
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createUserForm(User $user, $method)
    {
        $form = $this->createForm(new UserType(), $user, array(
            'method' => $method,
        ));

        return $form;
    }

    /**
     * Returns an User document.
     *
     * @param string $groupId An User document id
     *
     * @return \EventHorizon\SecurityBundle\Document\User
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getUserObject($userId)
    {
        $user = $this->dm->getRepository('EventHorizonSecurityBundle:User')->find($userId);

        if (!($user)) {
            $this->message = "EventHorizonWorkWatcherBundle:UserManagement:getUserObject id=".$userId." userId=".$this->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find User document.');
        }

        return $user;
    }

    /**
     * Lists all User documents.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/", name="user_management_index")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:UserManagement:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $limit = $request->query->get('limit', 20);
        $page = $request->query->get('page', 1);
        $query = $request->query->get('query');

        $users = $this->dm->getRepository('EventHorizonSecurityBundle:User')->getUsersBySearchV1($query);

        $pagination = $this->paginator->paginate($users, $page, $limit);

        return array(
            'pagination' => $pagination,
            'query' => $query,
        );
    }

    /**
     * Displays a form to create a new User document.
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/new", name="user_management_new")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:UserManagement:new.html.twig")
     */
    public function newAction()
    {
        $user = $this->userManager->create();
        $form = $this->createUserForm($user, 'POST');

        return array(
            'form' => $form->createView(),
            'user' => $user,
        );
    }

    /**
     * Creates a new User document.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("POST")
     * @Route("/new", name="user_management_create")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:UserManagement:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $user = $this->userManager->create();
        $form = $this->createUserForm($user, 'POST');
        $form->handleRequest($request);

        if (true === $form->isValid()) {
            $this->userManager->save($user);
            $this->addFlash('success', $this->translator->trans('user_created'));

            return $this->redirectToRoute('user_management_index');
        }

        return array(
            'form' => $form->createView(),
            'user' => $user,
        );
    }

    /**
     * Displays a form to edit an existing User document.
     *
     * @param string $id An User document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{id}/edit", name="user_management_edit")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:UserManagement:edit.html.twig")
     */
    public function editAction($id)
    {
        $user = $this->getUserObject($id);
        $editForm = $this->createUserForm($user, 'PUT');

        return array(
            'form' => $editForm->createView(),
            'user' => $user,
        );
    }

    /**
     * Edits an existing User document.
     *
     * @param Request $request A Request instance
     * @param string  $id      An User document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("PUT")
     * @Route("/{id}/edit", name="user_management_update")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template("EventHorizonThemeBundle:UserManagement:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $user = $this->getUserObject($id);
        $editForm = $this->createUserForm($user, 'PUT');
        $editForm->handleRequest($request);

        if (true === $editForm->isValid()) {
            $this->userManager->save($user);
            $this->addFlash('success', $this->translator->trans('user_updated'));

            return $this->redirectToRoute('user_management_index');
        }

        return array(
            'form' => $editForm->createView(),
            'user' => $user,
        );
    }
}
