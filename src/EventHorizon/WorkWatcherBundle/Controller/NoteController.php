<?php

namespace EventHorizon\WorkWatcherBundle\Controller;

use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use EventHorizon\WorkWatcherBundle\Document\Note;
use EventHorizon\WorkWatcherBundle\Form\NoteType;

/**
 * Note controller.
 *
 * @Route("/note")
 */
class NoteController extends Controller
{
    use \EventHorizon\WorkWatcherBundle\Controller\Traits\GetComputerObjectTrait;
    use \EventHorizon\WorkWatcherBundle\Controller\Traits\GetReportObjectTrait;

    /**
     * @var \Doctrine\Bundle\MongoDBBundle\ManagerRegistry
     *
     * @DI\Inject("doctrine_mongodb")
     */
    private $dm;

    /**
     * @var \EventHorizon\LogBundle\Service\Log
     *
     * @DI\Inject("event_horizon_log")
     */
    private $log;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\NoteManager
     *
     * @DI\Inject("event_horizon_work_watcher_note_manager")
     */
    private $noteManager;

    /**
     * @var \Knp\Component\Pager\Paginator
     *
     * @DI\Inject("knp_paginator")
     */
    private $paginator;

    /**
     * @var \EventHorizon\WorkWatcherBundle\Document\ReportManager
     *
     * @DI\Inject("event_horizon_work_watcher_report_manager")
     */
    private $reportManager;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContext
     *
     * @DI\Inject("security.context")
     */
    private $security;

    /**
     * @var \Symfony\Bundle\FrameworkBundle\Translation\Translator
     *
     * @DI\Inject("translator")
     */
    private $translator;

    /**
     * Creates a form for a Note document.
     *
     * @param Note   $note                  A Note instance
     * @param string $method                Form submit method
     * @param bool   $disableCsrfProtection Disable CSRF protection
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createNoteForm(Note $note, $method, $disableCsrfProtection = false)
    {
        $options = array(
            'method' => $method,
        );

        if (true === $disableCsrfProtection) {
            $options['csrf_protection'] = false;
        }

        $form = $this->createForm(new NoteType(), $note, $options);

        return $form;
    }

    /**
     * Creates a Computer document.
     *
     * @param Request $request A Request instance
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Note
     */
    private function createNoteObject(Request $request)
    {
        $reportId = $request->request->get('report_id');
        $request->request->remove('report_id');
        $report = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->find($reportId);
        $note = $this->noteManager->create($report, $this->getUser());
        $report->setNote($note);
        $createForm = $this->createNoteForm($note, 'POST', true);
        $createForm->handleRequest($request);

        if (true === $createForm->isValid()) {
            $this->noteManager->save($note);
            $this->reportManager->save($report);

            return $note;
        }

        return;
    }

    /**
     * Returns a Note document.
     *
     * @param string $noteId A Note document id
     *
     * @return \EventHorizon\WorkWatcherBundle\Document\Note
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getNoteObject($noteId)
    {
        $note = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Note')->find($noteId);

        if (null === $note) {
            $this->message = "EventHorizonWorkWatcherBundle:Note:getNoteObject id=".$noteId." userId=".$this->getUser()->getId();
            $this->log->logNotice($this->message);

            throw $this->createNotFoundException('Unable to find Note document.');
        }

        return $note;
    }

    /**
     * Lists all Note documents.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Cache(maxage="0", smaxage="0")
     *
     * @Method("GET")
     * @Route("/", name="note_index")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Note:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $computerId = $request->query->get('computer_id');
        $limit = $request->query->get('limit', 20);
        $page = $request->query->get('page', 1);
        $query = $request->query->get('query');
        $results = $this->noteManager->retriveComputersAndNotes($this->getUser(), $computerId, $query);
        $pagination = $this->paginator->paginate($results['notes'], $page, $limit);

        return array(
            'computerId' => $computerId,
            'computers' => $results['computers'],
            'pagination' => $pagination,
            'query' => $query,
        );
    }

    /**
     * Creates a new Note document.
     *
     * @param Request $request A Request instance
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("POST")
     * @Route("/new", name="note_create")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     */
    public function createAction(Request $request)
    {
        $note = $this->createNoteObject($request);

        $response = new Response();
        if ($note) {
            $response->setContent(json_encode(array(
                'code' => 200,
                'noteId' => $note->getId(),
            )));
            $response->setStatusCode(200);
        } else {
            $response->setStatusCode(404);
        }
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Displays a form to edit an existing Note document.
     *
     * @param string $id A Note document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{id}/edit", name="note_edit")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Note:edit.html.twig")
     */
    public function editAction($id)
    {
        $note = $this->getNoteObject($id);
        $editForm = $this->createNoteForm($note, 'PUT');

        return array(
            'form' => $editForm->createView(),
            'note' => $note,
        );
    }

    /**
     * Edits an existing Note document.
     *
     * @param Request $request A Request instance
     * @param string  $id      A Note document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("PUT")
     * @Route("/{id}/edit", name="note_update")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Note:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $note = $this->getNoteObject($id);
        $editForm = $this->createNoteForm($note, 'PUT');
        $editForm->handleRequest($request);

        if (true === $editForm->isValid()) {
            $this->noteManager->save($note);
            $this->addFlash('success', $this->translator->trans('note_updated'));

            return $this->redirectToRoute('note_show', array('id' => $note->getId()));
        }

        return array(
            'form' => $editForm->createView(),
            'note' => $note,
        );
    }

    /**
     * Shows note.
     *
     * @param string $id A Note document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{id}/show", name="note_show")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Note:show.html.twig")
     */
    public function showAction($id)
    {
        $note = $this->getNoteObject($id);

        return array(
            'note' => $note,
        );
    }

    /**
     * Shows summary of notes.
     *
     * @param Request $request    A Request instance
     * @param string  $computerId A Computer document id
     *
     * @return \Symfony\Component\HttpFoundation\Response A Response instance
     *
     * @Method("GET")
     * @Route("/{computerId}/show-summary", name="note_show_summary")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     * @Template("EventHorizonThemeBundle:Note:showSummary.html.twig")
     */
    public function showSummaryAction(Request $request, $computerId)
    {
        $computer = $this->getComputerObject($computerId);
        $dateEnd = $request->query->get('date_end');
        $dateStart = $request->query->get('date_start');
        $results = $this->noteManager->retriveNotes($computerId, $dateEnd, $dateStart);

        return array(
            'computer' => $computer,
            'dateEnd' => $results['dateEnd'],
            'dateStart' => $results['dateStart'],
            'notes' => $results['notes'],
        );
    }

    /**
     * Deletes a Note document.
     *
     * @param string $id A Note document id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse A RedirectResponse instance
     *
     * @Method("GET")
     * @Route("/{id}/delete", name="note_delete")
     * @Security("has_role('ROLE_ADMIN') or has_role('ROLE_OBSERVER')")
     */
    public function deleteAction($id)
    {
        $note = $this->getNoteObject($id);
        $report = $this->getReportObject($note->getReport()->getId());
        $report->clearNote();
        $this->reportManager->save($report);
        $this->noteManager->delete($note);
        $this->addFlash('success', $this->translator->trans('note_deleted'));

        return $this->redirectToRoute('note_index');
    }
}
