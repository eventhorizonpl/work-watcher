<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use EventHorizon\CoreBundle\Tests\AbstractDoctrineTestCase;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\ComputerManager;
use EventHorizon\WorkWatcherBundle\Document\NoteManager;
use EventHorizon\WorkWatcherBundle\Document\ReportManager;

class NoteManagerTest extends AbstractDoctrineTestCase
{
    private $security;
    private $validator;

    public function setUp()
    {
        static::$kernel = null;

        parent::setUp();

        $this->security = static::$kernel->getContainer()->get('security.context');
        $this->validator = static::$kernel->getContainer()->get('validator');
    }

    protected function tearDown()
    {
        unset($this->security);
        unset($this->validator);

        parent::tearDown();
    }

    public function testConstruct()
    {
        $noteManager = new NoteManager($this->dm, $this->security);

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\NoteManager', $noteManager);
    }

    public function fixtures()
    {
        $name = md5(microtime().rand(1, 1000));
        $user = new User();
        $user->setEmail("$name@$name.com");
        $user->setPlainPassword("$name");
        $user->setUsername("$name");
        $this->dm->persist($user);

        $computerManager = new ComputerManager($this->dm, $this->security);
        $computer = $computerManager->create($user);
        $computerManager->save($computer);

        $reportManager = new ReportManager($this->dm, $this->validator);

        $tmpfname = tempnam("/tmp", "png");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/6/60/F-test.png"));

        $date = 'test date';
        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');
        $hostname = 'test hostname';
        $ip = 'test ip';
        $time = 'test time';
        $username = 'test username';

        $report = $reportManager->createReport($computer, $date, $file, $hostname, $ip, $time, $username);
        $reportManager->save($report);

        return array(
            'computer' => $computer,
            'report' => $report,
            'user' => $user,
        );
    }

    public function testCreate()
    {
        $fixtures = $this->fixtures();

        $noteManager = new NoteManager($this->dm, $this->security);
        $note = $noteManager->create($fixtures['report'], $fixtures['user']);
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note);
    }

    public function testSave()
    {
        $fixtures = $this->fixtures();

        $noteManager = new NoteManager($this->dm, $this->security);
        $note = $noteManager->create($fixtures['report'], $fixtures['user']);
        $this->assertNull($noteManager->save($note));
    }

    public function testDelete()
    {
        $fixtures = $this->fixtures();

        $noteManager = new NoteManager($this->dm, $this->security);
        $note = $noteManager->create($fixtures['report'], $fixtures['user']);
        $this->assertNull($noteManager->save($note));
        $this->assertNull($noteManager->delete($note));
    }
}
