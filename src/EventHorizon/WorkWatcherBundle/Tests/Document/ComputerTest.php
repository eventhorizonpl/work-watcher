<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractTestCase;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Note;

class ComputerTest extends AbstractTestCase
{
    public function testConstruct()
    {
        $computer = new Computer();

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer);
    }

    public function testToString()
    {
        $name = "test name";

        $computer = new Computer();
        $computer->setName($name);

        $this->assertEquals($name, $computer->__toString());
    }

    public function testGetClientDelay()
    {
        $clientDelay = 1;
        $computer = new Computer();
        $this->assertEquals(600, $computer->getClientDelay());
        $computer->setClientDelay($clientDelay);
        $this->assertEquals($clientDelay, $computer->getClientDelay());
    }

    public function testSetClientDelay()
    {
        $clientDelay = 1;
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setClientDelay($clientDelay));
        $this->assertEquals($clientDelay, $computer->getClientDelay());
    }

    public function testGetDescription()
    {
        $description = "test description";
        $computer = new Computer();
        $this->assertNull($computer->getDescription());
        $computer->setDescription($description);
        $this->assertEquals($description, $computer->getDescription());
    }

    public function testSetDescription()
    {
        $description = "test description";
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setDescription($description));
        $this->assertEquals($description, $computer->getDescription());
    }

    public function testGetGroup()
    {
        $group = new Group('');
        $computer = new Computer();
        $this->assertNull($computer->getGroup());
        $computer->setGroup($group);
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Document\Group', $computer->getGroup());
        $this->assertEquals($group, $computer->getGroup());
    }

    public function testSetGroup()
    {
        $group = new Group('');
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setGroup($group));
        $this->assertEquals($group, $computer->getGroup());
    }

    public function testGetId()
    {
        $computer = new Computer();
        $this->assertNull($computer->getId());
    }

    public function testGetKey()
    {
        $key = "test key";
        $computer = new Computer();
        $this->assertNull($computer->getKey());
        $computer->setKey($key);
        $this->assertEquals($key, $computer->getKey());
    }

    public function testSetKey()
    {
        $key = "test key";
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setKey($key));
        $this->assertEquals($key, $computer->getKey());
    }

    public function testGetName()
    {
        $name = "test name";
        $computer = new Computer();
        $this->assertNull($computer->getName());
        $computer->setName($name);
        $this->assertEquals($name, $computer->getName());
    }

    public function testSetName()
    {
        $name = "test name";
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setName($name));
        $this->assertEquals($name, $computer->getName());
    }

    public function testAddNote()
    {
        $computer = new Computer();
        $note = new Note();
        $this->assertCount(0, $computer->getNotes());
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->addNote($note));
        $this->assertCount(1, $computer->getNotes());
    }

    public function testGetNotes()
    {
        $computer = new Computer();
        $this->assertInstanceOf('\Doctrine\Common\Collections\Collection', $computer->getNotes());
        $this->assertCount(0, $computer->getNotes());
    }

    public function testRemoveNote()
    {
        $computer = new Computer();
        $note = new Note();
        $this->assertCount(0, $computer->getNotes());
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->addNote($note));
        $this->assertCount(1, $computer->getNotes());
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->removeNote($note));
        $this->assertCount(0, $computer->getNotes());
    }

    public function testDecNoteCounter()
    {
        $computer = new Computer();
        $this->assertEquals(0, $computer->getNoteCounter());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $computer->decNoteCounter());
        $this->assertEquals(-1, $computer->getNoteCounter());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $computer->decNoteCounter());
        $this->assertEquals(-2, $computer->getNoteCounter());
    }

    public function testGetNoteCounter()
    {
        $computer = new Computer();
        $this->assertEquals(0, $computer->getNoteCounter());
    }

    public function testIncNoteCounter()
    {
        $computer = new Computer();
        $this->assertEquals(0, $computer->getNoteCounter());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $computer->incNoteCounter());
        $this->assertEquals(1, $computer->getNoteCounter());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $computer->incNoteCounter());
        $this->assertEquals(2, $computer->getNoteCounter());
    }

    public function testGetOverwriteClientConfiguration()
    {
        $overwriteClientConfiguration = true;
        $computer = new Computer();
        $this->assertFalse($computer->getOverwriteClientConfiguration());
        $computer->setOverwriteClientConfiguration($overwriteClientConfiguration);
        $this->assertEquals($overwriteClientConfiguration, $computer->getOverwriteClientConfiguration());
    }

    public function testSetOverwriteClientConfiguration()
    {
        $overwriteClientConfiguration = true;
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setOverwriteClientConfiguration($overwriteClientConfiguration));
        $this->assertEquals($overwriteClientConfiguration, $computer->getOverwriteClientConfiguration());
    }

    public function testGetUser()
    {
        $user = new User();
        $computer = new Computer();
        $this->assertNull($computer->getUser());
        $computer->setUser($user);
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Document\User', $computer->getUser());
        $this->assertEquals($user, $computer->getUser());
    }

    public function testSetUser()
    {
        $user = new User();
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setUser($user));
        $this->assertEquals($user, $computer->getUser());
    }

    public function testGetCreatedBy()
    {
        $user = "test user 1";
        $computer = new Computer();
        $this->assertNull($computer->getCreatedBy());
        $computer->setCreatedBy($user);
        $this->assertEquals($user, $computer->getCreatedBy());
    }

    public function testSetCreatedBy()
    {
        $user = "test user 1";
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setCreatedBy($user));
        $this->assertEquals($user, $computer->getCreatedBy());
    }

    public function testGetUpdatedBy()
    {
        $user = "test user 1";
        $computer = new Computer();
        $this->assertNull($computer->getUpdatedBy());
        $computer->setUpdatedBy($user);
        $this->assertEquals($user, $computer->getUpdatedBy());
    }

    public function testSetUpdatedBy()
    {
        $user = "test user 1";
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setUpdatedBy($user));
        $this->assertEquals($user, $computer->getUpdatedBy());
    }

    public function testGetCreatedAt()
    {
        $date = new \DateTime();
        $computer = new Computer();
        $this->assertNull($computer->getCreatedAt());
        $computer->setCreatedAt($date);
        $this->assertEquals($date, $computer->getCreatedAt());
    }

    public function testSetCreatedAt()
    {
        $date = new \DateTime();
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setCreatedAt($date));
        $this->assertEquals($date, $computer->getCreatedAt());
    }

    public function testGetUpdatedAt()
    {
        $date = new \DateTime();
        $computer = new Computer();
        $this->assertNull($computer->getUpdatedAt());
        $computer->setUpdatedAt($date);
        $this->assertEquals($date, $computer->getUpdatedAt());
    }

    public function testSetUpdatedAt()
    {
        $date = new \DateTime();
        $computer = new Computer();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer->setUpdatedAt($date));
        $this->assertEquals($date, $computer->getUpdatedAt());
    }

    public function testSetFromArray()
    {
        $description = "test description";
        $key = "test key";
        $name = "test name";
        $group = new Group('');
        $user = new User();

        $data = array(
            'group' => $group,
            'description' => $description,
            'key' => $key,
            'name' => $name,
            'user' => $user,
        );

        $computer = new Computer();
        $computer->setFromArray($data);

        $this->assertNull($computer->getId());
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Document\Group', $computer->getGroup());
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Document\User', $computer->getUser());
        $this->assertInstanceOf('\Doctrine\Common\Collections\Collection', $computer->getNotes());
        $this->assertEquals($description, $computer->getDescription());
        $this->assertEquals($key, $computer->getKey());
        $this->assertEquals($name, $computer->getName());
    }
}
