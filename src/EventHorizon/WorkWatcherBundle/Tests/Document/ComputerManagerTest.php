<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractDoctrineTestCase;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\ComputerManager;

class ComputerManagerTest extends AbstractDoctrineTestCase
{
    private $security;

    public function setUp()
    {
        parent::setUp();

        $this->security = static::$kernel->getContainer()->get('security.context');
    }

    protected function tearDown()
    {
        unset($this->security);

        parent::tearDown();
    }

    public function testConstruct()
    {
        $computerManager = new ComputerManager($this->dm, $this->security);

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\ComputerManager', $computerManager);
    }

    public function fixtures()
    {
        $name = md5(microtime().rand(1, 1000));
        $user = new User();
        $user->setEmail("$name@$name.com");
        $user->setPlainPassword("$name");
        $user->setUsername("$name");
        $this->dm->persist($user);

        return array(
            'user' => $user,
        );
    }

    public function testCreate()
    {
        $fixtures = $this->fixtures();
        $computerManager = new ComputerManager($this->dm, $this->security);
        $computer = $computerManager->create($fixtures['user']);

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Computer', $computer);
        $this->assertInstanceOf('EventHorizon\SecurityBundle\Document\User', $computer->getUser());
        $this->assertEquals($fixtures['user'], $computer->getUser());
    }

    public function testSave()
    {
        $fixtures = $this->fixtures();
        $computerManager = new ComputerManager($this->dm, $this->security);
        $computer = $computerManager->create($fixtures['user']);

        $this->assertNull($computerManager->save($computer));
    }

    public function testDelete()
    {
        $fixtures = $this->fixtures();
        $computerManager = new ComputerManager($this->dm, $this->security);
        $computer = $computerManager->create($fixtures['user']);

        $this->assertNull($computerManager->save($computer));
        $this->assertNull($computerManager->delete($computer));
    }
}
