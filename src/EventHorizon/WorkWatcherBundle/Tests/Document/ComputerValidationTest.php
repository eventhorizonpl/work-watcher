<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractKernelTestCase;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;

class ComputerValidationTest extends AbstractKernelTestCase
{
    private $validator;

    public function setUp()
    {
        parent::setUp();

        $this->validator = static::$kernel->getContainer()->get('validator');
    }

    protected function tearDown()
    {
        unset($this->validator);

        parent::tearDown();
    }

    /**
     * @dataProvider getValidTestData
     */
    public function testValidation1($formData)
    {
        $computer = new Computer();
        $computer->setFromArray($formData);

        $error = $this->validator->validate($computer);

        $this->assertEquals(0, $error->count());
    }

    public function getValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'group' => new Group('test'),
                    'description' => 'description',
                    'key' => 'key',
                    'name' => 'name',
                    'user' => new User(),
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'description' => 'description',
                    'key' => 'key',
                    'name' => 'name',
                    'user' => new User(),
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'key' => 'key',
                    'name' => 'name',
                    'user' => new User(),
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'key' => 'key',
                    'name' => 'name',
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
        );
    }

    /**
     * @dataProvider getNotValidTestData
     */
    public function testValidation2($formData)
    {
        $computer = new Computer();
        $computer->setFromArray($formData);

        $error = $this->validator->validate($computer);

        $this->assertGreaterThan(0, $error->count());
    }

    public function getNotValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'key' => null,
                    'name' => 'name',
                ),
            ),
            array(
                'data' => array(
                    'key' => null,
                    'name' => null,
                ),
            ),
        );
    }
}
