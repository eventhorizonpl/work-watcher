<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractTestCase;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Note;
use EventHorizon\WorkWatcherBundle\Document\Report;

class NoteTest extends AbstractTestCase
{
    public function testGetComputer()
    {
        $computer = new Computer();
        $note = new Note();
        $this->assertNull($note->getComputer());
        $note->setComputer($computer);
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $note->getComputer());
        $this->assertEquals($computer, $note->getComputer());
    }

    public function testSetComputer()
    {
        $computer = new Computer();
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setComputer($computer));
        $this->assertEquals($computer, $note->getComputer());
    }

    public function testGetContent()
    {
        $content = "test content";
        $note = new Note();
        $this->assertNull($note->getContent());
        $note->setContent($content);
        $this->assertEquals($content, $note->getContent());
    }

    public function testSetContent()
    {
        $content = "test content";
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setContent($content));
        $this->assertEquals($content, $note->getContent());
    }

    public function testGetReport()
    {
        $report = new Report();
        $note = new Note();
        $this->assertNull($note->getReport());
        $note->setReport($report);
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Report', $note->getReport());
        $this->assertEquals($report, $note->getReport());
    }

    public function testSetReport()
    {
        $report = new Report();
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setReport($report));
        $this->assertEquals($report, $note->getReport());
    }

    public function testGetTitle()
    {
        $title = "test title";
        $note = new Note();
        $this->assertNull($note->getTitle());
        $note->setTitle($title);
        $this->assertEquals($title, $note->getTitle());
    }

    public function testSetTitle()
    {
        $title = "test title";
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setTitle($title));
        $this->assertEquals($title, $note->getTitle());
    }

    public function testGetUser()
    {
        $user = new User();
        $note = new Note();
        $this->assertNull($note->getUser());
        $note->setUser($user);
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Document\User', $note->getUser());
        $this->assertEquals($user, $note->getUser());
    }

    public function testSetUser()
    {
        $user = new User();
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setUser($user));
        $this->assertEquals($user, $note->getUser());
    }

    public function testGetCreatedBy()
    {
        $user = "test user 1";
        $note = new Note();
        $this->assertNull($note->getCreatedBy());
        $note->setCreatedBy($user);
        $this->assertEquals($user, $note->getCreatedBy());
    }

    public function testSetCreatedBy()
    {
        $user = "test user 1";
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setCreatedBy($user));
        $this->assertEquals($user, $note->getCreatedBy());
    }

    public function testGetUpdatedBy()
    {
        $user = "test user 1";
        $note = new Note();
        $this->assertNull($note->getUpdatedBy());
        $note->setUpdatedBy($user);
        $this->assertEquals($user, $note->getUpdatedBy());
    }

    public function testSetUpdatedBy()
    {
        $user = "test user 1";
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setUpdatedBy($user));
        $this->assertEquals($user, $note->getUpdatedBy());
    }

    public function testGetCreatedAt()
    {
        $date = new \DateTime();
        $note = new Note();
        $this->assertNull($note->getCreatedAt());
        $note->setCreatedAt($date);
        $this->assertEquals($date, $note->getCreatedAt());
    }

    public function testSetCreatedAt()
    {
        $date = new \DateTime();
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setCreatedAt($date));
        $this->assertEquals($date, $note->getCreatedAt());
    }

    public function testGetUpdatedAt()
    {
        $date = new \DateTime();
        $note = new Note();
        $this->assertNull($note->getUpdatedAt());
        $note->setUpdatedAt($date);
        $this->assertEquals($date, $note->getUpdatedAt());
    }

    public function testSetUpdatedAt()
    {
        $date = new \DateTime();
        $note = new Note();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Note', $note->setUpdatedAt($date));
        $this->assertEquals($date, $note->getUpdatedAt());
    }

    public function testSetFromArray()
    {
        $content = "test content";
        $title = "test title";
        $computer = new Computer();
        $report = new Report();
        $user = new User();

        $data = array(
            'content' => $content,
            'title' => $title,
            'computer' => $computer,
            'report' => $report,
            'user' => $user,
        );

        $note = new Note();
        $note->setFromArray($data);

        $this->assertNull($note->getId());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $note->getComputer());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Report', $note->getReport());
        $this->assertInstanceOf('\EventHorizon\SecurityBundle\Document\User', $note->getUser());
        $this->assertEquals($content, $note->getContent());
        $this->assertEquals($title, $note->getTitle());
    }
}
