<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractKernelTestCase;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Note;
use EventHorizon\WorkWatcherBundle\Document\Report;

class ReportValidationTest extends AbstractKernelTestCase
{
    private $validator;

    public function setUp()
    {
        parent::setUp();

        $this->validator = static::$kernel->getContainer()->get('validator');
    }

    protected function tearDown()
    {
        unset($this->validator);

        parent::tearDown();
    }

    /**
     * @dataProvider getValidTestData
     */
    public function testValidation1($formData)
    {
        $report = new Report();
        $report->setFromArray($formData);

        $error = $this->validator->validate($report);

        $this->assertEquals(0, $error->count());
    }

    public function getValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'computer' => new Computer(),
                    'note' => new Note(),
                    'date' => 'test date',
                    'hostname' => 'test hostname',
                    'ip' => 'test ip',
                    'time' => 'test time',
                    'username' => 'test username',
                ),
            ),
            array(
                'data' => array(
                    'note' => new Note(),
                    'date' => 'test date',
                    'hostname' => 'test hostname',
                    'ip' => 'test ip',
                    'time' => 'test time',
                    'username' => 'test username',
                ),
            ),
            array(
                'data' => array(
                    'date' => 'test date',
                    'hostname' => 'test hostname',
                    'ip' => 'test ip',
                    'time' => 'test time',
                    'username' => 'test username',
                ),
            ),
        );
    }

    /**
     * @dataProvider getNotValidTestData
     */
    public function testValidation2($formData)
    {
        $report = new Report();
        $report->setFromArray($formData);

        $error = $this->validator->validate($report);

        $this->assertGreaterThan(0, $error->count());
    }

    public function getNotValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'hostname' => 'test hostname',
                    'ip' => 'test ip',
                    'time' => 'test time',
                    'username' => 'test username',
                ),
            ),
            array(
                'data' => array(
                    'date' => 'test date',
                    'ip' => 'test ip',
                    'time' => 'test time',
                    'username' => 'test username',
                ),
            ),
            array(
                'data' => array(
                    'date' => 'test date',
                    'hostname' => 'test hostname',
                    'time' => 'test time',
                    'username' => 'test username',
                ),
            ),
            array(
                'data' => array(
                    'date' => 'test date',
                    'hostname' => 'test hostname',
                    'ip' => 'test ip',
                    'username' => 'test username',
                ),
            ),
            array(
                'data' => array(
                    'date' => 'test date',
                    'hostname' => 'test hostname',
                    'ip' => 'test ip',
                    'time' => 'test time',
                ),
            ),
        );
    }
}
