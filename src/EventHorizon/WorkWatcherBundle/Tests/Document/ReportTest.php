<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractTestCase;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Note;
use EventHorizon\WorkWatcherBundle\Document\Report;

class ReportTest extends AbstractTestCase
{
    public function testToString()
    {
        $date = new \DateTime();
        $ip = "test ip";

        $report = new Report();
        $report->setDate($date->format("Y-m-d"));
        $report->setIp($ip);

        $this->assertEquals($ip.' '.$date->format("Y-m-d"), $report->__toString());
    }

    public function testGetComputer()
    {
        $computer = new Computer();
        $report = new Report();
        $this->assertNull($report->getComputer());
        $report->setComputer($computer);
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $report->getComputer());
        $this->assertEquals($computer, $report->getComputer());
    }

    public function testSetComputer()
    {
        $computer = new Computer();
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setComputer($computer));
        $this->assertEquals($computer, $report->getComputer());
    }

    public function testGetDate()
    {
        $date = "test date";
        $report = new Report();
        $this->assertNull($report->getDate());
        $report->setDate($date);
        $this->assertEquals($date, $report->getDate());
    }

    public function testSetDate()
    {
        $date = "test date";
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setDate($date));
        $this->assertEquals($date, $report->getDate());
    }

    public function testGetHostname()
    {
        $hostname = "test hostname";
        $report = new Report();
        $this->assertNull($report->getHostname());
        $report->setHostname($hostname);
        $this->assertEquals($hostname, $report->getHostname());
    }

    public function testSetHostname()
    {
        $hostname = "test hostname";
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setHostname($hostname));
        $this->assertEquals($hostname, $report->getHostname());
    }

    public function testGetIp()
    {
        $ip = "test ip";
        $report = new Report();
        $this->assertNull($report->getIp());
        $report->setIp($ip);
        $this->assertEquals($ip, $report->getIp());
    }

    public function testSetIp()
    {
        $ip = "test ip";
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setIp($ip));
        $this->assertEquals($ip, $report->getIp());
    }

    public function testClearNote()
    {
        $note = new Note();
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setNote($note));
        $this->assertEquals($note, $report->getNote());
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->clearNote());
        $this->assertNull($report->getNote());
    }

    public function testGetNote()
    {
        $note = new Note();
        $report = new Report();
        $this->assertNull($report->getNote());
        $report->setNote($note);
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Note', $report->getNote());
        $this->assertEquals($note, $report->getNote());
    }

    public function testSetNote()
    {
        $note = new Note();
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setNote($note));
        $this->assertEquals($note, $report->getNote());
    }

    public function testGetTime()
    {
        $time = "test time";
        $report = new Report();
        $this->assertNull($report->getTime());
        $report->setTime($time);
        $this->assertEquals($time, $report->getTime());
    }

    public function testSetTime()
    {
        $time = "test time";
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setTime($time));
        $this->assertEquals($time, $report->getTime());
    }

    public function testGetUsername()
    {
        $username = "test username";
        $report = new Report();
        $this->assertNull($report->getUsername());
        $report->setUsername($username);
        $this->assertEquals($username, $report->getUsername());
    }

    public function testSetUsername()
    {
        $username = "test username";
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setUsername($username));
        $this->assertEquals($username, $report->getUsername());
    }

    public function testGetCreatedAt()
    {
        $date = new \DateTime();
        $report = new Report();
        $this->assertNull($report->getCreatedAt());
        $report->setCreatedAt($date);
        $this->assertEquals($date, $report->getCreatedAt());
    }

    public function testSetCreatedAt()
    {
        $date = new \DateTime();
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setCreatedAt($date));
        $this->assertEquals($date, $report->getCreatedAt());
    }

    public function testGetUpdatedAt()
    {
        $date = new \DateTime();
        $report = new Report();
        $this->assertNull($report->getUpdatedAt());
        $report->setUpdatedAt($date);
        $this->assertEquals($date, $report->getUpdatedAt());
    }

    public function testSetUpdatedAt()
    {
        $date = new \DateTime();
        $report = new Report();
        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report->setUpdatedAt($date));
        $this->assertEquals($date, $report->getUpdatedAt());
    }

    public function testSetFromArray()
    {
        $computer = new Computer();
        $note = new Note();
        $date = "test date";
        $hostname = "test hostname";
        $ip = "test ip";
        $time = "test time";
        $username = "test username";

        $data = array(
            'computer' => $computer,
            'note' => $note,
            'date' => $date,
            'hostname' => $hostname,
            'ip' => $ip,
            'time' => $time,
            'username' => $username,
        );

        $report = new Report();
        $report->setFromArray($data);

        $this->assertNull($report->getId());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Computer', $report->getComputer());
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\Document\Note', $report->getNote());
        $this->assertEquals($date, $report->getDate());
        $this->assertEquals($hostname, $report->getHostname());
        $this->assertEquals($ip, $report->getIp());
        $this->assertEquals($time, $report->getTime());
        $this->assertEquals($username, $report->getUsername());
    }
}
