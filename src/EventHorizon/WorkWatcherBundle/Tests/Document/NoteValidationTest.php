<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use EventHorizon\CoreBundle\Tests\AbstractKernelTestCase;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Note;
use EventHorizon\WorkWatcherBundle\Document\Report;

class NoteValidationTest extends AbstractKernelTestCase
{
    private $validator;

    public function setUp()
    {
        parent::setUp();

        $this->validator = static::$kernel->getContainer()->get('validator');
    }

    protected function tearDown()
    {
        unset($this->validator);

        parent::tearDown();
    }

    /**
     * @dataProvider getValidTestData
     */
    public function testValidation1($formData)
    {
        $note = new Note();
        $note->setFromArray($formData);

        $error = $this->validator->validate($note);

        $this->assertEquals(0, $error->count());
    }

    public function getValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'content' => 'content',
                    'title' => 'title',
                    'computer' => new Computer(),
                    'report' => new Report(),
                    'user' => new User(),
                ),
            ),
            array(
                'data' => array(
                    'content' => 'content',
                    'title' => 'title',
                ),
            ),
        );
    }

    /**
     * @dataProvider getNotValidTestData
     */
    public function testValidation2($formData)
    {
        $note = new Note();
        $note->setFromArray($formData);

        $error = $this->validator->validate($note);

        $this->assertGreaterThan(0, $error->count());
    }

    public function getNotValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'content' => null,
                    'title' => 'title',
                ),
            ),
            array(
                'data' => array(
                    'content' => null,
                    'title' => null,
                ),
            ),
        );
    }
}
