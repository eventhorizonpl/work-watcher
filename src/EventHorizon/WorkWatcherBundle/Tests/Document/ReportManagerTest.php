<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Document;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use EventHorizon\CoreBundle\Tests\AbstractDoctrineTestCase;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\ComputerManager;
use EventHorizon\WorkWatcherBundle\Document\ReportManager;

class ReportManagerTest extends AbstractDoctrineTestCase
{
    private $security;
    private $validator;

    public function setUp()
    {
        static::$kernel = null;

        parent::setUp();

        $this->security = static::$kernel->getContainer()->get('security.context');
        $this->validator = static::$kernel->getContainer()->get('validator');
    }

    protected function tearDown()
    {
        unset($this->security);
        unset($this->validator);

        parent::tearDown();
    }

    public function testConstruct()
    {
        $reportManager = new ReportManager($this->dm, $this->validator);

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\ReportManager', $reportManager);
    }

    public function fixtures()
    {
        $name = md5(microtime().rand(1, 1000));
        $user = new User();
        $user->setEmail("$name@$name.com");
        $user->setPlainPassword("$name");
        $user->setUsername("$name");
        $this->dm->persist($user);

        $computerManager = new ComputerManager($this->dm, $this->security);
        $computer = $computerManager->create($user);
        $computerManager->save($computer);

        return array(
            'computer' => $computer,
            'user' => $user,
        );
    }

    public function testCreateReport1()
    {
        $fixtures = $this->fixtures();

        $reportManager = new ReportManager($this->dm, $this->validator);

        $tmpfname = tempnam("/tmp", "png");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/6/60/F-test.png"));

        $date = 'test date';
        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');
        $hostname = 'test hostname';
        $ip = 'test ip';
        $time = 'test time';
        $username = 'test username';

        $report = $reportManager->createReport($fixtures['computer'], $date, $file, $hostname, $ip, $time, $username);

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report);
        $this->assertEquals($date, $report->getDate());
        $this->assertEquals($hostname, $report->getHostname());
        $this->assertEquals($ip, $report->getIp());
        $this->assertEquals($time, $report->getTime());
        $this->assertEquals($username, $report->getUsername());
    }

    public function testCreateReport2()
    {
        $fixtures = $this->fixtures();

        $reportManager = new ReportManager($this->dm, $this->validator);

        $tmpfname = tempnam("/tmp", "jpg");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/b/bd/Test.jpg"));

        $date = 'test date';
        $file = new UploadedFile($tmpfname, 'test.jpg', 'image/jpeg');
        $hostname = 'test hostname';
        $ip = 'test ip';
        $time = 'test time';
        $username = 'test username';

        $report = $reportManager->createReport($fixtures['computer'], $date, $file, $hostname, $ip, $time, $username);

        $this->assertInstanceOf('EventHorizon\WorkWatcherBundle\Document\Report', $report);
        $this->assertEquals($date, $report->getDate());
        $this->assertEquals($hostname, $report->getHostname());
        $this->assertEquals($ip, $report->getIp());
        $this->assertEquals($time, $report->getTime());
        $this->assertEquals($username, $report->getUsername());
    }

    public function testSave()
    {
        $fixtures = $this->fixtures();

        $reportManager = new ReportManager($this->dm, $this->validator);

        $tmpfname = tempnam("/tmp", "png");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/6/60/F-test.png"));

        $date = 'test date';
        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');
        $hostname = 'test hostname';
        $ip = 'test ip';
        $time = 'test time';
        $username = 'test username';

        $report = $reportManager->createReport($fixtures['computer'], $date, $file, $hostname, $ip, $time, $username);

        $date = 'test date 2';
        $hostname = 'test hostname 2';
        $ip = 'test ip 2';
        $time = 'test time 2';
        $username = 'test username 2';

        $report->setDate($date);
        $report->setHostname($hostname);
        $report->setIp($ip);
        $report->setTime($time);
        $report->setUsername($username);

        $this->assertNull($reportManager->save($report));

        $reportSaved = $this->dm->getRepository('EventHorizonWorkWatcherBundle:Report')->find($report->getId());

        $this->assertEquals($date, $reportSaved->getDate());
        $this->assertEquals($hostname, $reportSaved->getHostname());
        $this->assertEquals($ip, $reportSaved->getIp());
        $this->assertEquals($time, $reportSaved->getTime());
        $this->assertEquals($username, $reportSaved->getUsername());
    }
}
