<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Admin;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class GroupAdminTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/security/group/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Group List")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Name")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Roles")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Filtry")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/security/group/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $uniqueId = $this->getTableUniqueId();

        $link = $this->crawler->selectLink($uniqueId)->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ogólne informacje o grupie")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Uprawnienia")')->count() > 0);

        $form = $this->crawler->selectButton('Zapisz zmiany')->form();
        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("został pomyślnie zaktualizowany")')->count() > 0);
    }
}
