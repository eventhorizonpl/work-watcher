<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Admin;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class ComputerAdminTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/workwatcher/computer/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Computer List")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Group")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("User")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Description")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Key")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Name")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/workwatcher/computer/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $uniqueId = $this->getTableUniqueId();

        $link = $this->crawler->selectLink($uniqueId)->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ogólne informacje o komputerze")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Klucz")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa")')->count() > 0);

        $form = $this->crawler->selectButton('Zapisz zmiany')->form();
        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("został pomyślnie zaktualizowany")')->count() > 0);
    }
}
