<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Admin;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class NoteAdminTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/workwatcher/note/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Note List")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Computer")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Report")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("User")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Content")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Title")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/workwatcher/note/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $uniqueId = $this->getTableUniqueId();

        $link = $this->crawler->selectLink($uniqueId)->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ogólne informacje o notatce")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);

        $form = $this->crawler->selectButton('Zapisz zmiany')->form();
        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("został pomyślnie zaktualizowany")')->count() > 0);
    }
}
