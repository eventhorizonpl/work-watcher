<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Admin;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class ReportAdminTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/admin/eventhorizon/workwatcher/report/list');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Report List")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Id")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Computer")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Date")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Ip")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Filtry")')->count() > 0);
    }
}
