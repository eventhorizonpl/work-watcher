<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Repository;

use EventHorizon\CoreBundle\Tests\AbstractDoctrineTestCase;
use EventHorizon\SecurityBundle\Document\User;

class NoteRepositoryTest extends AbstractDoctrineTestCase
{
    public function testGetNotesByComputerIdAndUserAndSearchV1()
    {
        $computerId = 1;
        $user = new User();
        $search = "12345";
        $notes = $this->dm
            ->getRepository('EventHorizonWorkWatcherBundle:Note')
            ->getNotesByComputerIdAndUserAndSearchV1($computerId, $user, $search);

        $this->assertEquals(1, count($notes));
    }

    public function testGetComputersByGroupIdsAndSearchV1()
    {
        $computerId = 1;
        $search = "12345";
        $notes = $this->dm
            ->getRepository('EventHorizonWorkWatcherBundle:Note')
            ->getNotesByComputerIdAndSearchV1($computerId, $search);

        $this->assertEquals(1, count($notes));
    }
}
