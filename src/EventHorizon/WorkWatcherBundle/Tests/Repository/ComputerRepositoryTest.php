<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Repository;

use EventHorizon\CoreBundle\Tests\AbstractDoctrineTestCase;

class ComputerRepositoryTest extends AbstractDoctrineTestCase
{
    public function testGetComputersByGroupIdAndSearchV1()
    {
        $groupId = 1;
        $search = "12345";
        $computers = $this->dm
            ->getRepository('EventHorizonWorkWatcherBundle:Computer')
            ->getComputersByGroupIdAndSearchV1($groupId, $search);

        $this->assertEquals(1, count($computers));
    }

    public function testGetComputersByGroupIdsAndSearchV1()
    {
        $groupIds = array(1, 2, 3);
        $search = "12345";
        $computers = $this->dm
            ->getRepository('EventHorizonWorkWatcherBundle:Computer')
            ->getComputersByGroupIdsAndSearchV1($groupIds, $search);

        $this->assertEquals(1, count($computers));
    }
}
