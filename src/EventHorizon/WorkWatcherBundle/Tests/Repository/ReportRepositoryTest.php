<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Repository;

use EventHorizon\CoreBundle\Tests\AbstractDoctrineTestCase;

class ReportRepositoryTest extends AbstractDoctrineTestCase
{
    public function testGetComputersByGroupIdsAndSearchV1()
    {
        $beforeDate = new \DateTime(date("Y-m-d"));
        $beforeDate->modify('-10 days');
        $beforeDate->setTime(0, 0, 0);
        $reports = $this->dm
            ->getRepository('EventHorizonWorkWatcherBundle:Report')
            ->removeReportsByDateV1($beforeDate);

        $this->assertNull($reports['err']);
    }
}
