<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Command;

use EventHorizon\WorkWatcherBundle\Tests\BaseCommandTest;

class DeleteOldReportsCommandTest extends BaseCommandTest
{
    public function testRegeneratePages()
    {
        $client = self::createClient();
        $output = $this->runCommand($client, "workwatcher:delete-old-reports");

        $this->assertContains('Deleting old reports...', $output);
        $this->assertContains('Done.', $output);

        $output = $this->runCommand($client, "workwatcher:delete-old-reports --days=10");

        $this->assertContains('Deleting old reports...', $output);
        $this->assertContains('Done.', $output);

        $output = $this->runCommand($client, "workwatcher:delete-old-reports --days=abc");

        $this->assertContains('Deleting old reports...', $output);
        $this->assertContains('Done.', $output);

        $output = $this->runCommand($client, "workwatcher:delete-old-reports -d 10");

        $this->assertContains('Deleting old reports...', $output);
        $this->assertContains('Done.', $output);
    }
}
