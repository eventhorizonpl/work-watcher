<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Command;

use EventHorizon\WorkWatcherBundle\Tests\BaseCommandTest;

class RegeneratePagesCommandTest extends BaseCommandTest
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;

    public function setUp()
    {
        parent::setUp();

        $this->dm = static::$kernel->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    protected function tearDown()
    {
        unset($this->dm);

        parent::tearDown();
    }

    public function testRegeneratePages()
    {
        $client = self::createClient();
        $output = $this->runCommand($client, "workwatcher:regenerate-pages");

        $this->assertContains('Regenerating pages from fixtures...', $output);
        $this->assertContains('Done.', $output);
    }

    public function testRegenerateDeletedPages()
    {
        $locale = "pl";
        $name = "help";

        $page = $this->dm->getRepository('EventHorizonPageBundle:Page')->getPageByLocaleAndNameV1($locale, $name);
        $this->dm->remove($page);
        $this->dm->flush();

        $client = self::createClient();
        $output = $this->runCommand($client, "workwatcher:regenerate-pages");

        $this->assertContains('Regenerating pages from fixtures...', $output);
        $this->assertContains('Done.', $output);
    }

    public function testRegenerateAllDeletedPages()
    {
        $pages = $this->dm->getRepository('EventHorizonPageBundle:Page')->findAll();

        foreach ($pages as $page) {
            $this->dm->remove($page);
        }
        $this->dm->flush();

        $client = self::createClient();
        $output = $this->runCommand($client, "workwatcher:regenerate-pages");

        $this->assertContains('Regenerating pages from fixtures...', $output);
        $this->assertContains('Done.', $output);
    }
}
