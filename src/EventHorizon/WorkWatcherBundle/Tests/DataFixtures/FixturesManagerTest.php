<?php

namespace EventHorizon\WorkWatcherBundle\Tests\DataFixtures;

use EventHorizon\CoreBundle\Tests\AbstractTestCase;
use EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager;

class FixturesManagerTest extends AbstractTestCase
{
    public function testConstructor()
    {
        $fixturesManager = new FixturesManager(0, false, false, 0, 0, 0, 0);

        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager);
    }

    public function testGettersAndSetters()
    {
        $computerFixtures = 1;
        $developmentFixtures = false;
        $faker = false;
        $groupFixtures = 2;
        $noteFixtures = 3;
        $reportFixtures = 4;
        $userFixtures = 5;

        $fixturesManager = new FixturesManager($computerFixtures, $developmentFixtures, $faker, $groupFixtures, $noteFixtures, $reportFixtures, $userFixtures);

        $this->assertEquals($computerFixtures, $fixturesManager->getComputerFixtures());
        $this->assertEquals($developmentFixtures, $fixturesManager->getDevelopmentFixtures());
        $this->assertEquals($faker, $fixturesManager->getFaker());
        $this->assertEquals($groupFixtures, $fixturesManager->getGroupFixtures());
        $this->assertEquals($noteFixtures, $fixturesManager->getNoteFixtures());
        $this->assertEquals($reportFixtures, $fixturesManager->getReportFixtures());
        $this->assertEquals($userFixtures, $fixturesManager->getUserFixtures());

        $computerFixtures = 11;
        $developmentFixtures = true;
        $faker = true;
        $groupFixtures = 22;
        $noteFixtures = 33;
        $reportFixtures = 44;
        $userFixtures = 55;

        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setComputerFixtures($computerFixtures));
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setDevelopmentFixtures($developmentFixtures));
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setFaker($faker));
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setGroupFixtures($groupFixtures));
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setNoteFixtures($noteFixtures));
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setReportFixtures($reportFixtures));
        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DataFixtures\FixturesManager', $fixturesManager->setUserFixtures($userFixtures));

        $this->assertEquals($computerFixtures, $fixturesManager->getComputerFixtures());
        $this->assertEquals($developmentFixtures, $fixturesManager->getDevelopmentFixtures());
        $this->assertEquals($faker, $fixturesManager->getFaker());
        $this->assertEquals($groupFixtures, $fixturesManager->getGroupFixtures());
        $this->assertEquals($noteFixtures, $fixturesManager->getNoteFixtures());
        $this->assertEquals($reportFixtures, $fixturesManager->getReportFixtures());
        $this->assertEquals($userFixtures, $fixturesManager->getUserFixtures());
    }
}
