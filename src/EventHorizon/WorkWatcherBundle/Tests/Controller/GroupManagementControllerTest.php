<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class GroupManagementControllerTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Zarządzanie grupami')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zarządzanie grupami")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa grupy")')->count() > 0);

        $form = $this->crawler->selectButton('Szukaj')->form();

        $form['query'] = 'default group';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("default group")')->count() > 0);
    }

    public function testNew()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Zarządzanie grupami')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Nowa grupa')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Nowa grupa")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa grupy")')->count() > 0);

        $form = $this->crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_securitybundle_group_type[name]'] = '';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość nie powinna być pusta")')->count() > 0);

        $form['eventhorizon_securitybundle_group_type[name]'] = 'Nazwa grupy test';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zarządzanie grupami")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa grupy")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa została utworzona.")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Zarządzanie grupami')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Edycja grupy")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa grupy")')->count() > 0);

        $link = $this->crawler->selectLink('Powrót')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $this->crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_securitybundle_group_type[name]'] = '';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość nie powinna być pusta")')->count() > 0);

        $form['eventhorizon_securitybundle_group_type[name]'] = 'default group';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zarządzanie grupami")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa grupy")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa została zaktualizowana.")')->count() > 0);
    }

    public function testEditException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/group_management/12345/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Group document")')->count() > 0);
    }
}
