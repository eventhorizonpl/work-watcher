<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class Api11ControllerTest extends BaseUiTest
{
    public function fixtures()
    {
        $group = new Group('');
        $group->setName('test123');
        $group->setRoles(array('ROLE_OBSERVER'));
        $this->dm->persist($group);

        $user = new User();
        $user->addGroup($group);
        $user->setEmail('test@test.com');
        $user->setUsername('test');
        $user->setPlainPassword('test');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));
        $this->dm->persist($user);

        $computer = new Computer();
        $computer->setGroup($group);
        $computer->setUser($user);
        $computer->setDescription('description');
        $computer->setKey('key');
        $computer->setName('name');
        $this->dm->persist($computer);

        $this->dm->flush();

        return $computer;
    }

    public function testComputerShow11Action()
    {
        $computer = $this->fixtures();

        $this->crawler = $this->client->request('GET', '/api/v1.1/computers/'.$computer->getId());
        $computerResult = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('data', $computerResult);
        $this->assertEquals($computer->getId(), $computerResult['data']['id']);
        $this->assertEquals($computer->getDescription(), $computerResult['data']['description']);
        $this->assertEquals($computer->getName(), $computerResult['data']['name']);
    }

    public function testReportSubmit11PngAction()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $tmpfname = tempnam("/tmp", "png");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/6/60/F-test.png"));

        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data, array('file' => $file));
        $reportResult = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmit11WrongPngAction()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $tmpfname = tempnam("/tmp", "jpg");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/b/bd/Test.jpg"));

        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data, array('file' => $file));
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmit11JpgAction()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $tmpfname = tempnam("/tmp", "jpg");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/b/bd/Test.jpg"));

        $file = new UploadedFile($tmpfname, 'test.jpg', 'image/jpeg');

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data, array('file' => $file));
        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException1()
    {
        $this->crawler = $this->client->request('POST', '/api/v1.1/reports');
        $this->assertTrue($this->crawler->filter('html:contains("computer_id was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException2()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
        );

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data);
        $this->assertTrue($this->crawler->filter('html:contains("computer_key was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException3()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
        );

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data);
        $this->assertTrue($this->crawler->filter('html:contains("date was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException4()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data);
        $this->assertTrue($this->crawler->filter('html:contains("hostname was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException5()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data);
        $this->assertTrue($this->crawler->filter('html:contains("time was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException6()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data);
        $this->assertTrue($this->crawler->filter('html:contains("username was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException7()
    {
        $data = array(
            'computer_id' => 1,
            'computer_key' => 2,
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.1/reports', $data);
        $this->assertTrue($this->crawler->filter('html:contains("computer was not found")')->count() > 0);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function test11ApiAction()
    {
        $this->crawler = $this->client->request('GET', '/api/v1.1/test');
        $computerResult = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('data', $computerResult);
        $this->assertArrayHasKey('status', $computerResult['data']);
        $this->assertEquals("ok", $computerResult['data']['status']);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
