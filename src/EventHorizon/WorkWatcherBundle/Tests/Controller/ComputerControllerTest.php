<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class ComputerControllerTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Lista komputerów")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Klucz")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa")')->count() > 0);
    }

    public function testIndexObserver()
    {
        $this->loginAsObserver();

        $link = $this->crawler->selectLink('Lista komputerów')->link();

        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $group = $this->dm->getRepository('EventHorizonSecurityBundle:Group')->findOneBy(array('name' => 'default group'));

        $form = $this->crawler->selectButton('Szukaj')->form();
        $form['group_id']->select($group->getId());

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Lista komputerów")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Klucz")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa")')->count() > 0);
    }

    public function testNew()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Nowy komputer')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Nowy komputer")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Klucz")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa")')->count() > 0);

        $form = $this->crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_workwatcherbundle_computer_type[name]'] = 'Nazwa komputera test';
        $form['eventhorizon_workwatcherbundle_computer_type[key]'] = 'Klucz test';
        $form['eventhorizon_workwatcherbundle_computer_type[description]'] = 'Opis test';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość jest nieprawidłowa")')->count() > 0);

        $form['eventhorizon_workwatcherbundle_computer_type[key]'] = 'Klucztest';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("ID komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Informacje o komputerze zostały dodane.")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Edycja informacji o komputerze")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Klucz")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Opis")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupa")')->count() > 0);

        $link = $this->crawler->selectLink('Powrót')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);

        $form = $this->crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_workwatcherbundle_computer_type[name]'] = 'Nazwa komputera test edycja';
        $form['eventhorizon_workwatcherbundle_computer_type[key]'] = 'Klucz test edycja';
        $form['eventhorizon_workwatcherbundle_computer_type[description]'] = 'Opis test edycja';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość jest nieprawidłowa")')->count() > 0);

        $form['eventhorizon_workwatcherbundle_computer_type[key]'] = 'Klucztestedycja';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("ID komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Informacje o komputerze zostały zaktualizowane.")')->count() > 0);
    }

    public function testEditException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/computer/12345/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Computer document")')->count() > 0);
    }

    public function testDelete()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('2')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Usuń')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("ID komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Informacje o komputerze zostały usunięte.")')->count() > 0);
    }

    public function testPrint()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Lista komputerów do wydruku')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa komputera test")')->count() > 0);
    }
}
