<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class StatisticsControllerTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Statystyki')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $this->crawler->selectButton('Szukaj')->form();
        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Statystyki")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Liczba komputerów w systemie")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Liczba notatek w systemie")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Liczba raportów w systemie")')->count() > 0);
    }

    public function testNotesPerComputer()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Statystyki')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Notatki na komputer')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Notatki na komputer")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Liczba notatek")')->count() > 0);
    }
}
