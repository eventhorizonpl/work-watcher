<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class UserManagementControllerTest extends BaseUiTest
{
    public function testIndex()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Zarządzanie użytkownikami')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zarządzanie użytkownikami")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Aktywność konta")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Uprawnienia")')->count() > 0);

        $form = $this->crawler->selectButton('Szukaj')->form();

        $form['query'] = 'admin';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("admin@admin.com")')->count() > 0);
    }

    public function testNew()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Zarządzanie użytkownikami')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Nowy użytkownik')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Nowy użytkownik")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Powtórz hasło")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Uprawnienia")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupy")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Konto włączone")')->count() > 0);

        $form = $this->crawler->selectButton('Zapisz')->form();

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość nie powinna być pusta.")')->count() > 0);

        $form['eventhorizon_securitybundle_user_type[username]'] = 'Nazwa użytkownika test';
        $form['eventhorizon_securitybundle_user_type[email]'] = 'test@test.com';
        $form['eventhorizon_securitybundle_user_type[plainPassword][first]'] = 'password test';
        $form['eventhorizon_securitybundle_user_type[plainPassword][second]'] = 'password test';
        $form['eventhorizon_securitybundle_user_type[enabled]']->select(0);

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zarządzanie użytkownikami")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Konto użytkownika zostało utworzone.")')->count() > 0);
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Zarządzanie użytkownikami')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Edycja użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("E-mail")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Hasło")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Powtórz hasło")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Uprawnienia")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Grupy")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Konto włączone")')->count() > 0);

        $link = $this->crawler->selectLink('Powrót')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $this->crawler->selectButton('Zapisz')->form();

        $username = $form['eventhorizon_securitybundle_user_type[username]']->getValue();

        $form['eventhorizon_securitybundle_user_type[username]'] = '';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość nie powinna być pusta.")')->count() > 0);

        $form['eventhorizon_securitybundle_user_type[username]'] = $username;

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zarządzanie użytkownikami")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Konto użytkownika zostało zaktualizowane.")')->count() > 0);
    }

    public function testEditException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/user_management/12345/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find User document")')->count() > 0);
    }
}
