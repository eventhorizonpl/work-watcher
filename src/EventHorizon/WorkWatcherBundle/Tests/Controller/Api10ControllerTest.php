<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class Api10ControllerTest extends BaseUiTest
{
    public function fixtures()
    {
        $group = new Group('');
        $group->setName('test123');
        $group->setRoles(array('ROLE_OBSERVER'));
        $this->dm->persist($group);

        $user = new User();
        $user->addGroup($group);
        $user->setEmail('test@test.com');
        $user->setUsername('test');
        $user->setPlainPassword('test');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));
        $this->dm->persist($user);

        $computer = new Computer();
        $computer->setGroup($group);
        $computer->setUser($user);
        $computer->setDescription('description');
        $computer->setKey('key');
        $computer->setName('name');
        $this->dm->persist($computer);

        $this->dm->flush();

        return $computer;
    }

    public function testReportSubmit10PngAction()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $tmpfname = tempnam("/tmp", "png");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/6/60/F-test.png"));

        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data, array('file' => $file));
        $this->assertEquals("{\"status\":\"ok\"}", $this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmit10WrongPngAction()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $tmpfname = tempnam("/tmp", "jpg");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/b/bd/Test.jpg"));

        $file = new UploadedFile($tmpfname, 'test.png', 'image/png');

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data, array('file' => $file));
        $this->assertEquals(500, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmit10JpgAction()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $tmpfname = tempnam("/tmp", "jpg");
        file_put_contents($tmpfname, file_get_contents("http://upload.wikimedia.org/wikipedia/en/b/bd/Test.jpg"));

        $file = new UploadedFile($tmpfname, 'test.jpg', 'image/jpeg');

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data, array('file' => $file));
        $this->assertEquals("{\"status\":\"ok\"}", $this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException1()
    {
        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit');
        $this->assertTrue($this->crawler->filter('html:contains("computer_id was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException2()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
        );

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data);
        $this->assertTrue($this->crawler->filter('html:contains("computer_key was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException3()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
        );

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data);
        $this->assertTrue($this->crawler->filter('html:contains("date was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException4()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data);
        $this->assertTrue($this->crawler->filter('html:contains("hostname was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException5()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data);
        $this->assertTrue($this->crawler->filter('html:contains("time was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException6()
    {
        $computer = $this->fixtures();
        $data = array(
            'computer_id' => $computer->getId(),
            'computer_key' => $computer->getKey(),
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data);
        $this->assertTrue($this->crawler->filter('html:contains("username was not specified")')->count() > 0);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

    public function testReportSubmitActionException7()
    {
        $data = array(
            'computer_id' => 1,
            'computer_key' => 2,
            'date' => '2014-I-01',
            'hostname' => 'hostname',
            'time' => '01:01',
            'username' => 'username',
        );

        $this->crawler = $this->client->request('POST', '/api/v1.0/report/submit', $data);
        $this->assertTrue($this->crawler->filter('html:contains("computer was not found")')->count() > 0);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function test10ApiAction()
    {
        $this->crawler = $this->client->request('GET', '/api/v1.0/test');
        $this->assertEquals("{\"status\":\"ok\"}", $this->client->getResponse()->getContent());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}
