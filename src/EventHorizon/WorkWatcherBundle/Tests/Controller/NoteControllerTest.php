<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Report;
use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class NoteControllerTest extends BaseUiTest
{
    public function fixtures()
    {
        $group = new Group('');
        $group->setName('test123');
        $group->setRoles(array('ROLE_OBSERVER'));
        $this->dm->persist($group);

        $user = new User();
        $user->addGroup($group);
        $user->setEmail('test@test.com');
        $user->setUsername('test');
        $user->setPlainPassword('test');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));
        $this->dm->persist($user);

        $computer = new Computer();
        $computer->setGroup($group);
        $computer->setUser($user);
        $computer->setDescription('description');
        $computer->setKey('key');
        $computer->setName('name');
        $this->dm->persist($computer);

        $report = new Report();
        $report->setComputer($computer);
        $report->setDate('2014-01-01');
        $report->setHostname('hostname');
        $report->setIp('127.0.0.1');
        $report->setTime('01:01');
        $report->setUsername('username');
        $report->setFile('web/apple-touch-icon.png');
        $report->setFilename('test.png');
        $report->setMimeType('image/png');
        $this->dm->persist($report);

        $this->dm->flush();

        return $report;
    }

    public function testIndex()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista moich notatek')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Lista moich notatek")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);
    }

    public function testIndexObserver()
    {
        $this->loginAsObserver();

        $link = $this->crawler->selectLink('Lista moich notatek')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Lista moich notatek")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);
    }

    public function testCreate1()
    {
        $this->loginAsAdmin();

        $report = $this->fixtures();

        $data = array(
            'report_id' => $report->getId(),
            '_method' => 'post',
            'eventhorizon_workwatcherbundle_note_type' => array(
                'content' => 'test content',
                'title' => 'test title',
            ),
        );

        $this->crawler = $this->client->request('POST', '/note/new', $data);
        $result = (array) json_decode($this->client->getResponse()->getContent());
        $this->assertArrayHasKey('code', $result);
        $this->assertArrayHasKey('noteId', $result);
        $this->assertEquals(200, $result['code']);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testCreate2()
    {
        $this->loginAsAdmin();

        $report = $this->fixtures();

        $data = array(
            'report_id' => $report->getId(),
            '_method' => 'post',
            'eventhorizon_workwatcherbundle_note_type' => array(
            ),
        );

        $this->crawler = $this->client->request('POST', '/note/new', $data);
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
    }

    public function testEdit()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista moich notatek')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Edycja notatki")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);

        $link = $this->crawler->selectLink('Powrót')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Edycja')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $form = $this->crawler->selectButton('Zapisz')->form();

        $form['eventhorizon_workwatcherbundle_note_type[title]'] = '';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Ta wartość nie powinna być pusta.")')->count() > 0);

        $form['eventhorizon_workwatcherbundle_note_type[title]'] = 'Tytuł test edcja';
        $form['eventhorizon_workwatcherbundle_note_type[content]'] = 'Treść test edcja';

        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Lista moich notatek")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść notatki została zaktualizowana.")')->count() > 0);
    }

    public function testEditException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/note/12345/edit');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Note document")')->count() > 0);
    }

    public function testShow()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista moich notatek')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Pokaż notatkę')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Zrzut ekranu")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("ID raportu")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("IP")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Data")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Opis komputera")')->count() > 0);
    }

    public function testDelete()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista moich notatek')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Usuń')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Lista moich notatek")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Notatka została usunięta.")')->count() > 0);
    }

    public function testDeleteException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/note/12345/delete');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Note document")')->count() > 0);
    }

    public function testShowSummary()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Zestawienie notatek')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zestawienie notatek")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Zestawienie notatek dla komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tytuł")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Treść")')->count() > 0);

        $form = $this->crawler->selectButton('Szukaj')->form();
        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Zestawienie notatek")')->count() > 0);
    }

    public function testShowSummaryException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/note/1234/show-summary');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Computer document")')->count() > 0);
    }
}
