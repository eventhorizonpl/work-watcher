<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Controller;

use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\SecurityBundle\Document\User;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Document\Report;
use EventHorizon\WorkWatcherBundle\Tests\BaseUiTest;

class ReportControllerTest extends BaseUiTest
{
    public function fixtures()
    {
        $group = new Group('');
        $group->setName('test123');
        $group->setRoles(array('ROLE_OBSERVER'));
        $this->dm->persist($group);

        $user = new User();
        $user->addGroup($group);
        $user->setEmail('test@test.com');
        $user->setUsername('test');
        $user->setPlainPassword('test');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));
        $this->dm->persist($user);

        $computer = new Computer();
        $computer->setGroup($group);
        $computer->setUser($user);
        $computer->setDescription('description');
        $computer->setKey('key');
        $computer->setName('name');
        $this->dm->persist($computer);

        $report = new Report();
        $report->setComputer($computer);
        $report->setDate('2014-01-01');
        $report->setHostname('hostname');
        $report->setIp('127.0.0.1');
        $report->setTime('01:01');
        $report->setUsername('username');
        $report->setFile('web/apple-touch-icon.png');
        $report->setFilename('test.png');
        $report->setMimeType('image/png');
        $this->dm->persist($report);

        $this->dm->flush();

        return $report;
    }

    public function fixtures2()
    {
        $group = new Group('');
        $group->setName('test123');
        $group->setRoles(array('ROLE_OBSERVER'));
        $this->dm->persist($group);

        $user = new User();
        $user->addGroup($group);
        $user->setEmail('test@test.com');
        $user->setUsername('test');
        $user->setPlainPassword('test');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));
        $this->dm->persist($user);

        $computer = new Computer();
        $computer->setGroup($group);
        $computer->setUser($user);
        $computer->setDescription('description');
        $computer->setKey('key');
        $computer->setName('name');
        $this->dm->persist($computer);

        $report = new Report();
        $report->setComputer($computer);
        $report->setDate('2014-01-01');
        $report->setHostname('hostname');
        $report->setIp('127.0.0.1');
        $report->setTime('01:01');
        $report->setUsername('username');
        $this->dm->persist($report);

        $this->dm->flush();

        return $report;
    }

    public function testFileShow()
    {
        $report = $this->fixtures();

        $this->loginAsAdmin();

        ob_start();
        $this->crawler = $this->client->request('GET', '/report/file-show/'.$report->getId());
        ob_end_clean();
        $this->assertRegExp('/report\/file-show\/'.$report->getId().'/', $this->crawler->serialize());
    }

    public function testFileShow2()
    {
        $report = $this->fixtures2();

        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/report/file-show/'.$report->getId());
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("report photo was not found")')->count() > 0);
    }

    public function testFileShowException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/report/file-show/12345');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Report document")')->count() > 0);
    }

    public function testShowReport()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Raporty')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Raporty")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Zestawienie raportów dla komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nowa notatka")')->count() > 0);

        $form = $this->crawler->selectButton('Szukaj')->form();
        $this->crawler = $this->client->submit($form);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Raporty")')->count() > 0);
    }

    public function testShowReportException()
    {
        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/report/12345/show-reports');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Unable to find Computer document")')->count() > 0);
    }

    public function testShowLiveView()
    {
        $this->loginAsAdmin();

        $link = $this->crawler->selectLink('Lista komputerów')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());

        $link = $this->crawler->selectLink('Na żywo')->link();
        $this->crawler = $this->client->click($link);
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("Na żywo")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Tryb \'na żywo\' dla komputera")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Raporty")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Zestawienie notatek")')->count() > 0);
    }

    public function testShowLiveViewFile()
    {
        $report = $this->fixtures();

        $this->loginAsAdmin();

        ob_start();
        $this->crawler = $this->client->request('GET', '/report/'.$report->getComputer()->getId().'/show-live-view-file');
        ob_end_clean();
        $this->assertRegExp('/report\/'.$report->getComputer()->getId().'\/show-live-view-file/', $this->crawler->serialize());
    }

    public function testShowLiveViewFile2()
    {
        $report = $this->fixtures2();

        $this->loginAsAdmin();

        $this->crawler = $this->client->request('GET', '/report/'.$report->getComputer()->getId().'/show-live-view-file');
        $this->assertEquals(404, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($this->crawler->filter('html:contains("report photo was not found")')->count() > 0);
    }
}
