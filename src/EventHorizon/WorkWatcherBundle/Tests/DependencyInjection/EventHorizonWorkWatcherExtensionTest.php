<?php

namespace EventHorizon\WorkWatcherBundle\Tests\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use EventHorizon\CoreBundle\Tests\AbstractTestCase;
use EventHorizon\WorkWatcherBundle\DependencyInjection\EventHorizonWorkWatcherExtension;

class EventHorizonWorkWatcherExtensionTest extends AbstractTestCase
{
    public function testLoad()
    {
        $loader = new EventHorizonWorkWatcherExtension();
        $config = array(
            'computer_fixtures' => 0,
            'development_fixtures' => false,
            'faker' => false,
            'group_fixtures' => 0,
            'note_fixtures' => 0,
            'report_fixtures' => 0,
            'user_fixtures' => 0,
        );
        $loader->load(array($config), new ContainerBuilder());

        $this->assertInstanceOf('\EventHorizon\WorkWatcherBundle\DependencyInjection\EventHorizonWorkWatcherExtension', $loader);
    }
}
