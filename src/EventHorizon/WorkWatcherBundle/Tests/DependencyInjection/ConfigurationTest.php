<?php

namespace EventHorizon\WorkWatcherBundle\Tests\DependencyInjection;

use Symfony\Bundle\WebProfilerBundle\DependencyInjection\Configuration;
use EventHorizon\CoreBundle\Tests\AbstractTestCase;

class ConfigurationTest extends AbstractTestCase
{
    public function testGetConfigTreeBuilder()
    {
        $configuration = new Configuration(array());
        $tree = $configuration->getConfigTreeBuilder();

        $this->assertInstanceOf('\Symfony\Component\Config\Definition\Builder\NodeParentInterface', $tree);
    }
}
