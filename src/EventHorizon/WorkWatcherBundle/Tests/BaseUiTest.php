<?php

namespace EventHorizon\WorkWatcherBundle\Tests;

use EventHorizon\CoreBundle\Tests\AbstractWebTestCase;

abstract class BaseUiTest extends AbstractWebTestCase
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;

    public function setUp()
    {
        parent::setUp();

        $this->client->followRedirects(true);

        $this->dm = static::$kernel->getContainer()->get('doctrine.odm.mongodb.document_manager');
    }

    protected function tearDown()
    {
        unset($this->dm);

        parent::tearDown();
    }

    public function getTableUniqueId()
    {
        $selector = 'table > tbody > tr > td > a';
        $el = $this->crawler->filter($selector);
        $uniqueId = trim($el->text());

        return $uniqueId;
    }

    public function loginAsAdmin()
    {
        $this->crawler = $this->client->request('GET', '/login');

        $this->assertTrue($this->crawler->filter('html:contains("Logowanie")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $this->crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'admin';
        $form['_password'] = 'admin';

        $this->crawler = $this->client->submit($form);
    }

    public function loginAsObserver()
    {
        $this->crawler = $this->client->request('GET', '/login');

        $this->assertTrue($this->crawler->filter('html:contains("Logowanie")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Nazwa użytkownika")')->count() > 0);
        $this->assertTrue($this->crawler->filter('html:contains("Hasło")')->count() > 0);

        $form = $this->crawler->selectButton('Zaloguj')->form();

        $form['_username'] = 'observer';
        $form['_password'] = 'observer';

        $this->crawler = $this->client->submit($form);
    }
}
