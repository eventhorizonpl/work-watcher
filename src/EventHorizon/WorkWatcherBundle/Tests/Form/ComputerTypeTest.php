<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\WorkWatcherBundle\Document\Computer;
use EventHorizon\WorkWatcherBundle\Form\ComputerType;

class ComputerTypeTest extends TypeTestCase
{
    /**
     * @dataProvider getValidTestData
     */
    public function testValidForm($formData)
    {
        $type = new ComputerType();
        $form = $this->factory->create($type);

        $object = new Computer();
        $object->setFromArray($formData);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function getValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'group' => new Group('test'),
                    'description' => 'description',
                    'key' => 'key',
                    'name' => 'name',
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'description' => 'description',
                    'key' => 'key',
                    'name' => 'name',
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'key' => 'key',
                    'name' => 'name',
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'name' => 'name',
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
            array(
                'data' => array(
                    'clientDelay' => 60,
                    'overwriteClientConfiguration' => true,
                ),
            ),
        );
    }
}
