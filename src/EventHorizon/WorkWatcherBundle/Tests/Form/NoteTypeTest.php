<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use EventHorizon\WorkWatcherBundle\Document\Note;
use EventHorizon\WorkWatcherBundle\Form\NoteType;

class NoteTypeTest extends TypeTestCase
{
    /**
     * @dataProvider getValidTestData
     */
    public function testValidForm($formData)
    {
        $type = new NoteType();
        $form = $this->factory->create($type);

        $object = new Note();
        $object->setFromArray($formData);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function getValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'content' => 'content',
                    'title' => 'title',
                ),
            ),
            array(
                'data' => array(
                    'title' => 'title',
                ),
            ),
            array(
                'data' => array(),
            ),
        );
    }
}
