<?php

namespace EventHorizon\WorkWatcherBundle\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use EventHorizon\SecurityBundle\Document\Group;
use EventHorizon\WorkWatcherBundle\Form\GroupType;

class GroupTypeTest extends TypeTestCase
{
    /**
     * @dataProvider getValidTestData
     */
    public function testValidForm($formData)
    {
        $type = new GroupType();
        $form = $this->factory->create($type, new Group(''));

        $object = new Group('');
        $object->setFromArray($formData);

        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function getValidTestData()
    {
        return array(
            array(
                'data' => array(
                    'name' => 'name',
                ),
            ),
            array(
                'data' => array(),
            ),
        );
    }
}
