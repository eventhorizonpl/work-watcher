<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use EventHorizon\WorkWatcherBundle\Document\Report;

class LoadReportData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $fixturesManager = $this->container->get('event_horizon_work_watcher_fixtures_manager');

        if (true === $fixturesManager->getDevelopmentFixtures()) {
            $faker = \Faker\Factory::create();

            for ($i = 1; $i <= $fixturesManager->getComputerFixtures(); $i++) {
                for ($j = 1; $j <= $fixturesManager->getReportFixtures(); $j++) {
                    $report = new Report();
                    $report->setComputer($manager->merge($this->getReference('computer-'.$i)));

                    $report->setDate(date("Y-m-d"));
                    $report->setTime(date("H:i:s"));
                    if (true === $fixturesManager->getFaker()) {
                        $report->setIp($faker->ipv4);
                        $report->setUsername($faker->userName);
                    } else {
                        $report->setIp("127.0.0.1");
                        $report->setUsername('test'.$i);
                    }

                    $manager->persist($report);

                    $this->addReference('report-'.$i.'-'.$j, $report);
                }
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 30;
    }
}
