<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use EventHorizon\WorkWatcherBundle\Document\Note;

class LoadNoteData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $fixturesManager = $this->container->get('event_horizon_work_watcher_fixtures_manager');

        if (true === $fixturesManager->getDevelopmentFixtures()) {
            $faker = \Faker\Factory::create();

            for ($i = 1; $i <= $fixturesManager->getComputerFixtures(); $i++) {
                for ($j = 1; $j <= $fixturesManager->getReportFixtures(); $j++) {
                    for ($k = 1; $k <= $fixturesManager->getNoteFixtures(); $k++) {
                        $note = new Note();
                        $note->setComputer($manager->merge($this->getReference('computer-'.$i)));
                        $note->setReport($manager->merge($this->getReference('report-'.$i.'-'.$j)));
                        $note->setUser($manager->merge($this->getReference('observer-user')));

                        if (true === $fixturesManager->getFaker()) {
                            $note->setContent($faker->text);
                            $note->setTitle($faker->text);
                        } else {
                            $note->setContent('content'.$i.'-'.$j.'-'.$k);
                            $note->setTitle('title'.$i.'-'.$j.'-'.$k);
                        }

                        $manager->persist($note);

                        $this->addReference('note-'.$i.'-'.$j.'-'.$k, $note);
                    }
                }
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 40;
    }
}
