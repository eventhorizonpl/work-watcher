<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EventHorizon\PageBundle\Document\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $page = new Page();

        $content = file_get_contents('misc/doc/help.pl.html');

        $page->setContent($content);
        $page->setName('help');
        $page->setLocale('pl');
        $page->setTitle('Pomoc');
        $page->setIsVisible(true);

        $manager->persist($page);

        $content = file_get_contents('misc/doc/install.pl.html');

        $page = new Page();
        $page->setContent($content);
        $page->setName('install');
        $page->setLocale('pl');
        $page->setTitle('Instalacja systemu');
        $page->setIsVisible(true);

        $manager->persist($page);

        $content = file_get_contents('misc/doc/license.pl.html');

        $page = new Page();
        $page->setContent($content);
        $page->setName('license');
        $page->setLocale('pl');
        $page->setTitle('Licencja');
        $page->setIsVisible(true);

        $manager->persist($page);

        $content = file_get_contents('misc/doc/requirements.pl.html');

        $page = new Page();
        $page->setContent($content);
        $page->setName('requirements');
        $page->setLocale('pl');
        $page->setTitle('WorkWatcher - wymagania sprzętowe/systemowe');
        $page->setIsVisible(true);

        $manager->persist($page);

        $content = file_get_contents('misc/doc/changelog.pl.html');

        $page = new Page();
        $page->setContent($content);
        $page->setName('changelog');
        $page->setLocale('pl');
        $page->setTitle('Lista zmian');
        $page->setIsVisible(true);

        $manager->persist($page);

        $content = file_get_contents('misc/doc/update.pl.html');

        $page = new Page();
        $page->setContent($content);
        $page->setName('update');
        $page->setLocale('pl');
        $page->setTitle('Aktualizacja');
        $page->setIsVisible(true);

        $manager->persist($page);

        $content = file_get_contents('misc/doc/faq.pl.html');

        $page = new Page();
        $page->setContent($content);
        $page->setName('faq');
        $page->setLocale('pl');
        $page->setTitle('Często zadawane pytania');
        $page->setIsVisible(true);

        $manager->persist($page);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
