<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use EventHorizon\SecurityBundle\Document\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->addGroup($manager->merge($this->getReference('default-group')));
        $user->setEmail('admin@admin.com');
        $user->setEnabled(true);
        $user->setPlainPassword('admin');
        $user->setRoles(array('ROLE_ADMIN', 'ROLE_OBSERVER', 'ROLE_USER'));
        $user->setUsername('admin');

        $manager->persist($user);

        $this->addReference('admin-user', $user);

        $user = new User();
        $user->addGroup($manager->merge($this->getReference('default-group')));
        $user->setEmail('observer@observer.com');
        $user->setEnabled(true);
        $user->setPlainPassword('observer');
        $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));
        $user->setUsername('observer');

        $manager->persist($user);

        $this->addReference('observer-user', $user);

        $fixturesManager = $this->container->get('event_horizon_work_watcher_fixtures_manager');

        if (true === $fixturesManager->getDevelopmentFixtures()) {
            $faker = \Faker\Factory::create();

            for ($i = 1; $i <= $fixturesManager->getUserFixtures(); $i++) {
                $user = new User();
                $user->addGroup($manager->merge($this->getReference('default-group')));
                if (true === $fixturesManager->getFaker()) {
                    $username = $faker->userName;
                    $user->setEmail($faker->email);
                    $user->setUsername($username);
                    $user->setPlainPassword($username);
                } else {
                    $user->setEmail('test'.$i.'@test'.$i.'.com');
                    $user->setUsername('test'.$i);
                    $user->setPlainPassword('test'.$i);
                }
                $user->setEnabled(true);
                $user->setRoles(array('ROLE_OBSERVER', 'ROLE_USER'));

                $manager->persist($user);

                $this->addReference('user-'.$i, $user);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 15;
    }
}
