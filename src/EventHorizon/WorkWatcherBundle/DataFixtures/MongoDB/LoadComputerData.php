<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use EventHorizon\WorkWatcherBundle\Document\Computer;

class LoadComputerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $fixturesManager = $this->container->get('event_horizon_work_watcher_fixtures_manager');

        if (true === $fixturesManager->getDevelopmentFixtures()) {
            $faker = \Faker\Factory::create();

            for ($i = 1; $i <= $fixturesManager->getComputerFixtures(); $i++) {
                $computer = new Computer();
                $computer->setGroup($manager->merge($this->getReference('default-group')));
                $computer->setUser($manager->merge($this->getReference('observer-user')));
                if (true === $fixturesManager->getFaker()) {
                    $computer->setDescription($faker->text);
                    $computer->setKey($faker->md5);
                    $computer->setName($faker->text(32));
                } else {
                    $computer->setDescription('description'.$i);
                    $computer->setKey('key'.$i);
                    $computer->setName('name'.$i);
                }

                $manager->persist($computer);

                $this->addReference('computer-'.$i, $computer);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}
