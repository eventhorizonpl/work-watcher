<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use EventHorizon\SecurityBundle\Document\Group;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $group = new Group('');
        $group->setName('default group');
        $group->setRoles(array('ROLE_OBSERVER'));

        $manager->persist($group);

        $this->addReference('default-group', $group);

        $fixturesManager = $this->container->get('event_horizon_work_watcher_fixtures_manager');

        if (true === $fixturesManager->getDevelopmentFixtures()) {
            $faker = \Faker\Factory::create();

            for ($i = 1; $i <= $fixturesManager->getGroupFixtures(); $i++) {
                $group = new Group('');
                if (true === $fixturesManager->getFaker()) {
                    $group->setName($faker->text(32));
                } else {
                    $group->setName('group'.$i);
                }
                $group->setRoles(array('ROLE_OBSERVER'));

                $manager->persist($group);

                $this->addReference('group-'.$i, $group);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}
