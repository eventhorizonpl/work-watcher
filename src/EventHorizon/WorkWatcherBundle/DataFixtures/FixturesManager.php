<?php

namespace EventHorizon\WorkWatcherBundle\DataFixtures;

class FixturesManager
{
    private $computerFixtures;
    private $developmentFixtures;
    private $faker;
    private $groupFixtures;
    private $noteFixtures;
    private $reportFixtures;
    private $userFixtures;

    public function __construct($computerFixtures, $developmentFixtures, $faker, $groupFixtures, $noteFixtures, $reportFixtures, $userFixtures)
    {
        $this->computerFixtures = $computerFixtures;
        $this->developmentFixtures = $developmentFixtures;
        $this->faker = $faker;
        $this->groupFixtures = $groupFixtures;
        $this->noteFixtures = $noteFixtures;
        $this->reportFixtures = $reportFixtures;
        $this->userFixtures = $userFixtures;
    }

    public function getComputerFixtures()
    {
        return $this->computerFixtures;
    }

    public function setComputerFixtures($computerFixtures)
    {
        $this->computerFixtures = $computerFixtures;

        return $this;
    }

    public function getDevelopmentFixtures()
    {
        return $this->developmentFixtures;
    }

    public function setDevelopmentFixtures($developmentFixtures)
    {
        $this->developmentFixtures = $developmentFixtures;

        return $this;
    }

    public function getFaker()
    {
        return $this->faker;
    }

    public function setFaker($faker)
    {
        $this->faker = $faker;

        return $this;
    }

    public function getGroupFixtures()
    {
        return $this->groupFixtures;
    }

    public function setGroupFixtures($groupFixtures)
    {
        $this->groupFixtures = $groupFixtures;

        return $this;
    }

    public function getNoteFixtures()
    {
        return $this->noteFixtures;
    }

    public function setNoteFixtures($noteFixtures)
    {
        $this->noteFixtures = $noteFixtures;

        return $this;
    }

    public function getReportFixtures()
    {
        return $this->reportFixtures;
    }

    public function setReportFixtures($reportFixtures)
    {
        $this->reportFixtures = $reportFixtures;

        return $this;
    }

    public function getUserFixtures()
    {
        return $this->userFixtures;
    }

    public function setUserFixtures($userFixtures)
    {
        $this->userFixtures = $userFixtures;

        return $this;
    }
}
