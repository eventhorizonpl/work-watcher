$(document).ready(function() {
    $('img.lazy').Lazy();

    $('[rel="popover"]').popover({
        html: true,
        trigger: 'hover',
    });

    $('.confirm_delete').confirmModal();

    $('.datepicker').datepicker({
        format: "yyyy-mm-dd"
    });
});
